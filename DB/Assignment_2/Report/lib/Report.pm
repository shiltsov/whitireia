package Report;
use strict;

use Dancer2;
use Dancer2::Plugin::Database;

our $VERSION = '0.1';

get '/' => sub {
    template 'index',{
        year  => 2007,
        month => 9,
    }
    
};

any '/report' => sub {
    my $sth = database->prepare('select * from spcreatereport(?,?);');
    $sth->execute(params->{year},params->{month});

    my $raw_results = $sth->fetchall_arrayref();
    my $clean_results = {};
    my $reporting_total = 0;
    foreach my $row (@$raw_results) {
        my ($purchaseorderid, $productnumber, $orderqty, $unitprice, $linetotal) = @$row;
        $clean_results->{$purchaseorderid} = {Total => 0, Products => []} unless exists $clean_results->{$purchaseorderid};
        
        $clean_results->{$purchaseorderid}->{Total} += $linetotal;
        $reporting_total += $linetotal;
        push($clean_results->{$purchaseorderid}->{Products}, [$productnumber, $orderqty, $unitprice, $linetotal]);
    }
    
    template 'index', {
            year                => params->{year},
            month               => params->{month},
            add_entry_url       => uri_for('/add'),
            results             => $clean_results,
            total               => $reporting_total,
        };
};

true;
