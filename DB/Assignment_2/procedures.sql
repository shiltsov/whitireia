DROP TABLE IF EXISTS PurchaseReport CASCADE;
CREATE TABLE PurchaseReport (
    PurchaseOrderID int, 
    productNumber varchar(25),
    OrderQty smallint, 
    UnitPrice numeric(19,4), 
    LineTotal numeric(19,4)
);

-- Usage example: SELECT * FROM spCreateReport(2007,9);
DROP FUNCTION IF EXISTS spCreateReport(integer, integer);
CREATE FUNCTION spCreateReport (integer, integer) RETURNS SETOF PurchaseReport AS $$
    use strict;
    my ($year, $month) = @_;

    #########     Queries    ##########
    
    my $orderheader_querry = "SELECT purchaseorderid 
        FROM purchasing_purchaseorderheader 
        WHERE EXTRACT(YEAR FROM orderdate) = $year 
            AND EXTRACT(MONTH FROM orderdate) = $month";
    
    my $order_detail_querry = 'SELECT orderqty, unitprice, linetotal, productid 
        FROM purchasing_purchaseorderdetail 
        WHERE PurchaseOrderID = $1';
    
    my $order_detail_plan = spi_prepare($order_detail_querry, 'integer');
        
    my $product_querry = 'SELECT productnumber 
        FROM production_product 
        WHERE productid = $1';
    
    my $product_plan = spi_prepare($product_querry, 'integer');
    
    
    #########      Actual code     #########
    
    my $orderheader_results = spi_query($orderheader_querry);
    
    while (defined (my $orderheader = spi_fetchrow($orderheader_results))) {
        my $purchase_order_id = $orderheader->{purchaseorderid};
        my $order_detail_results = spi_query_prepared($order_detail_plan, $purchase_order_id);

        while (defined (my $orderdetail = spi_fetchrow($order_detail_results))) {
            my $orderqty  = $orderdetail->{orderqty};
            my $unitprice = $orderdetail->{unitprice};
            my $linetotal = $orderdetail->{linetotal};
            my $productid = $orderdetail->{productid};

            my $product_results = spi_exec_prepared($product_plan, $productid);
            my $product_number = $product_results->{rows}->[0]->{productnumber}; 
            return_next {
                purchaseorderid => $purchase_order_id,  
                productnumber   => $product_number,
                orderqty        => $orderqty, 
                unitprice       => $unitprice,
                linetotal       => $linetotal
            };
        }
    }

    return undef;
$$ LANGUAGE plperl;

/* Performance testing */

DO $$
    my $i = 0;
    my $start_time = time();
    while ($i++ < 10000){
        spi_exec_query('select * FROM spCreateReport(2007,9)');
    }
    elog(NOTICE, time() - $start_time);
$$ LANGUAGE plperl;

DO $$
    my $i = 0;
    my $start_time = time();
    while ($i++ < 10000){
        spi_exec_query('select PurchaseOrderID, productNumber, OrderQty, UnitPrice, LineTotal
            FROM Purchasing_PurchaseOrderHeader 
                INNER JOIN Purchasing_PurchaseOrderDetail USING (PurchaseOrderID) 
                INNER JOIN production_product USING (productid)
                    WHERE EXTRACT(YEAR FROM OrderDate)=2007 and EXTRACT(MONTH FROM OrderDate)=9;');
    }
    elog(NOTICE, time() - $start_time);
$$ LANGUAGE plperl;

PREPARE report(int, int) AS
    select PurchaseOrderID, productNumber, OrderQty, UnitPrice, LineTotal
            FROM Purchasing_PurchaseOrderHeader 
                INNER JOIN Purchasing_PurchaseOrderDetail USING (PurchaseOrderID) 
                INNER JOIN production_product USING (productid)
                    WHERE EXTRACT(YEAR FROM OrderDate)=$1 and EXTRACT(MONTH FROM OrderDate)=$2;
    
EXECUTE report(2007, 9);

DO $$
    my $i = 0;
    my $start_time = time();
    while ($i++ < 10000){
        spi_exec_query('EXECUTE report(2007, 9)')
    }
    elog(NOTICE, time() - $start_time);
$$ LANGUAGE plperl;
