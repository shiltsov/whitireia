-- to be used with pg_prove
BEGIN;
SELECT plan(25);

------------------ TESTING EMPLOYEE ----------------------

PREPARE correct_employees AS INSERT INTO Employee (Forename, Surname, Start_Date, Manager, Position) VALUES  
    ('Boss', 'Big', 'January 8, 1999', NULL, 'Administrator'),
    ('Jane', 'Bloggs', 'January 8, 2012', (SELECT (Employee_ID) FROM Employee WHERE Forename = 'Big' AND Surname = 'Boss'), 'Administrator'),
    ('John', 'Doe', 'January 12, 2013', (SELECT (Employee_ID) FROM Employee WHERE Forename = 'Big' AND Surname = 'Boss'), 'Trainer'),
    ('Jane', 'Roe', 'January 11, 2010', (SELECT (Employee_ID) FROM Employee WHERE Forename = 'John' AND Surname = 'Doe'), 'Trainer'),
    ('Mary', 'Sue',  'January 13, 2011', (SELECT (Employee_ID) FROM Employee WHERE Forename = 'John' AND Surname = 'Doe'), 'Trainer');
SELECT lives_ok(
    'correct_employees',
    'Accepting proper employees'
);

PREPARE bad_employee_no_name AS INSERT INTO Employee (Surname, Start_Date, Manager, Position) VALUES  
    ('Karlsson', 'January 8, 2001', NULL, 'Trainer');
SELECT throws_ok(
    'bad_employee_no_name',
    '23502',
    'null value in column "forename" violates not-null constraint',
    'Name should be defined'
);

PREPARE bad_employee_bad_manager AS INSERT INTO Employee (Forename, Surname, Start_Date, Manager, Position) VALUES  
    ('Richard', 'Roe', 'January 8, 2001', 100500, 'Trainer');
SELECT throws_ok(
    'bad_employee_bad_manager',
    '23503',
    'insert or update on table "employee" violates foreign key constraint "fk_employee"',
    'Manager should be NULL or already defined in as Employee'
);


PREPARE bad_employee_bad_position AS INSERT INTO Employee (Forename, Surname, Start_Date, Manager, Position) VALUES  
    ('Richard', 'Roe', 'January 8, 2001', NULL, 'Batman');
SELECT throws_ok(
    'bad_employee_bad_position',
    '23514',
    'new row for relation "employee" violates check constraint "employee_position"',
    'Position should be Trainer or Administrator'
);

------------------ TESTING TRAINER ----------------------

PREPARE correct_trainers AS INSERT INTO Trainer (Employee_ID, Position, Certification, Level) VALUES  
    (((SELECT (Employee_ID) FROM Employee WHERE Forename = 'John' AND Surname = 'Doe')), 'Trainer', 'MCP', 'Trainer'),
    (((SELECT (Employee_ID) FROM Employee WHERE Forename = 'Jane' AND Surname = 'Roe')), 'Trainer', 'OCP', 'Consultant'),
    (((SELECT (Employee_ID) FROM Employee WHERE Forename = 'Mary' AND Surname = 'Sue')), 'Trainer', 'OCP', 'Trainer');
SELECT lives_ok(
    'correct_trainers',
    'Accepting proper trainers'
);

PREPARE not_existing_trainers AS INSERT INTO Trainer (Employee_ID, Position, Certification, Level) VALUES  
    (100501, 'Trainer', 'MCP', 'Trainer'),
    (100502, 'Trainer', 'OCP', 'Consultant');
SELECT throws_ok(
    'not_existing_trainers',
    '23503',
    'insert or update on table "trainer" violates foreign key constraint "fk1_trainer_employee"',
    'Not possible to set non exising people as Trainers'
);

PREPARE trainer_as_batman AS UPDATE Trainer SET Position = 'Batman!' WHERE Position = 'Trainer';
SELECT throws_ok(
    'trainer_as_batman',
    '23514',
    'new row for relation "trainer" violates check constraint "employee_position"',
    'Not possible to change Trainers to Batmen'
);

PREPARE admin_as_trainer AS INSERT INTO Trainer (Employee_ID, Position, Certification, Level) VALUES  
    (((SELECT (Employee_ID) FROM Employee WHERE Forename = 'Jane' AND Surname = 'Bloggs')), 'Trainer', 'OCP', 'Trainer');
SELECT throws_ok(
    'admin_as_trainer',
    '23503',
    'insert or update on table "trainer" violates foreign key constraint "fk2_trainer_employee"',
    'Not possible to set Administrator as a Trainer'
);

------------------ TESTING ADMINISTRATOR ----------------------

PREPARE correct_administrators AS INSERT INTO Administrator (Employee_ID, Position, Bonus) VALUES  
    (((SELECT (Employee_ID) FROM Employee WHERE Forename = 'Boss' AND Surname = 'Big')), 'Administrator', TRUE),
    (((SELECT (Employee_ID) FROM Employee WHERE Forename = 'Jane' AND Surname = 'Bloggs')), 'Administrator', FALSE);
SELECT lives_ok(
    'correct_administrators',
    'Accepting proper administrators'
);

PREPARE not_existing_administrators AS INSERT INTO Administrator (Employee_ID, Position, Bonus) VALUES  
    (100501, 'Administrator', TRUE),
    (100502, 'Administrator', FALSE);
SELECT throws_ok(
    'not_existing_administrators',
    '23503',
    'insert or update on table "administrator" violates foreign key constraint "fk1_administrator_employee"',
    'Not possible to set non exising people as Administrators'
);

PREPARE administrator_as_batman AS UPDATE Administrator SET Position = 'Batman!' WHERE Position = 'Administrator';
SELECT throws_ok(
    'administrator_as_batman',
    '23514',
    'new row for relation "administrator" violates check constraint "employee_position"',
    'Not possible to change Administrators to Batmen'
);


PREPARE trainer_as_admin AS INSERT INTO Administrator (Employee_ID, Position, Bonus) VALUES  
    ((SELECT (Employee_ID) FROM Employee WHERE position = 'Trainer' FETCH FIRST ROW ONLY), 'Administrator', FALSE);
SELECT throws_ok(
    'trainer_as_admin',
    '23503',
    'insert or update on table "administrator" violates foreign key constraint "fk2_administrator_employee"',
    'Not possible to set Trainer as a Administrator'
);

------------------ TESTING EVENT ----------------------

PREPARE correct_events AS INSERT INTO Event (Event_Name, Event_Date, Seats_Availible, Event_Cost, Specialisation, Level) VALUES  
    ('Microsoft for Dummies', 'April 3, 2015', 12, 998.99, 'Microsoft', 'Beginner'),
    ('Mastering Oracle', 'April 1, 2015', 31, 9998.99, 'Oracle', 'Advanced');
SELECT lives_ok(
    'correct_events',
    'Accepting proper events'
);

PREPARE incorrect_event AS INSERT INTO Event (Event_Name, Event_Date, Seats_Availible, Event_Cost, Specialisation, Level) VALUES  
    ('Migration to PostgreSQL', 'April 5, 2015', 256, 9.99, 'PostgreSQL', 'Intermediate');
SELECT throws_ok(
    'incorrect_event',
    '23514',
    'new row for relation "event" violates check constraint "event_specialisation"',
    'Not possible to set event for PostgreSQL'
);

------------------ TESTING EVENT_SESSION ----------------------

PREPARE correct_event_session AS INSERT INTO Event_Session (Session_Number, Event_ID, Employee_ID, Event_Specialisation, Trainer_Certification, Event_Level, Trainer_level) VALUES 
    (7, 
    (SELECT (Event_ID) FROM Event WHERE Specialisation = 'Microsoft' FETCH FIRST ROW ONLY), 
    (SELECT (Employee_ID) FROM Trainer WHERE Certification = 'MCP' FETCH FIRST ROW ONLY),
    'Microsoft',
    'MCP',
    'Beginner',
    'Trainer'
    );
SELECT lives_ok(
    'correct_event_session',
    'Accepting proper event_session'
);

PREPARE event_session_by_admin AS INSERT INTO Event_Session (Session_Number, Event_ID, Employee_ID, Event_Specialisation, Trainer_Certification, Event_Level, Trainer_level) VALUES 
    (666, 
    (SELECT (Event_ID) FROM Event  WHERE Specialisation = 'Microsoft' FETCH FIRST ROW ONLY), 
    (SELECT (Employee_ID) FROM Employee WHERE position = 'Administrator' FETCH FIRST ROW ONLY),
    'Microsoft',
    'MCP',
    'Beginner',
    'Trainer'
    );
SELECT throws_ok(
    'event_session_by_admin',
    '23503',
    'insert or update on table "event_session" violates foreign key constraint "fk_event_session_trainer"',
    'Not possible to set event session for administrator'
);

PREPARE event_session_spec_mismatch AS INSERT INTO Event_Session (Session_Number, Event_ID, Employee_ID, Event_Specialisation, Trainer_Certification, Event_Level, Trainer_Level) VALUES 
    (7, 
    (SELECT (Event_ID) FROM Event WHERE Specialisation = 'Microsoft' FETCH FIRST ROW ONLY), 
    (SELECT (Employee_ID) FROM Trainer WHERE Certification = 'OCP' AND Level = 'Consultant' FETCH FIRST ROW ONLY),
    'Microsoft',
    'OCP',
    'Beginner',
    'Consultant'
    );
SELECT throws_ok(
    'event_session_spec_mismatch',
    '23514',
    'new row for relation "event_session" violates check constraint "event_session_specialisation_match"',
    'Not possible to set Microsoft session for OCP Certified trainer'
);

PREPARE event_session_level_mismatch AS INSERT INTO Event_Session (Session_Number, Event_ID, Employee_ID, Event_Specialisation, Trainer_Certification, Event_Level, Trainer_Level) VALUES 
    (7, 
    (SELECT (Event_ID) FROM Event WHERE Level = 'Advanced' AND Specialisation = 'Oracle' FETCH FIRST ROW ONLY), 
    (SELECT (Employee_ID) FROM Trainer WHERE Level = 'Trainer' AND Certification = 'OCP' FETCH FIRST ROW ONLY),
    'Oracle',
    'OCP',
    'Advanced',
    'Trainer'    
    );
SELECT throws_ok(
    'event_session_level_mismatch',
    '23514',
    'new row for relation "event_session" violates check constraint "event_session_level_match"',
    'Not possible to set Advanced session for not Consultant trainer'
);

------------------ TESTING CLIENT ----------------------

PREPARE correct_clients AS INSERT INTO Client (Forename, Surname, Street, Suburb, City, Country, Post_Code, Phone, Business_Name, Employee_Total, Specialisation, Level) VALUES 
    ('Bilbo', 'Baggins', 'Bag End', 'Bagshot Row', 'Hobbiton', 'Shire', '100500','Palantir #7',NULL, NULL, 'Oracle', 'Advanced'),
    ('Frodo', 'Baggins', 'Bag End', 'Bagshot Row', 'Hobbiton', 'Shire', '100500','Palantir #13','Fellowship of the Ring', 9, 'Microsoft', 'Beginner'),
    ('The', 'Ring', 'Bag End', 'Bagshot Row', 'Hobbiton', 'Shire', '100500','Palantir #666',NULL, NULL, NULL, 'Advanced');
SELECT lives_ok(
    'correct_clients',
    'Accepting proper clients'
);

PREPARE oracle_to_sqlite AS UPDATE CLIENT SET Specialisation = 'SQLite' WHERE Specialisation = 'Oracle';
SELECT throws_ok(
    'oracle_to_sqlite',
    '23514',
    'new row for relation "client" violates check constraint "event_specialisation"',
    'Not possible to change Specialisation to SQLite'
);

PREPARE beginner_to_guru AS UPDATE CLIENT SET Level = 'Guru' WHERE Level = 'Beginner';
SELECT throws_ok(
    'beginner_to_guru',
    '23514',
    'new row for relation "client" violates check constraint "event_level"',
    'Not possible to change Level to Guru'
);

------------------ TESTING REGISTRATION ----------------------

PREPARE correct_registration AS INSERT INTO Registration (Registration_Date, Attendees, Employee_ID, Event_ID, Event_Level, Event_Specialisation, Client_ID, Client_Level,Client_Specialisation) VALUES 
    ('March 30, 2015', 7,
    (SELECT (Employee_ID) FROM Employee WHERE position = 'Administrator' FETCH FIRST ROW ONLY),
    (SELECT (Event_ID) FROM Event WHERE Level = 'Advanced' AND Specialisation = 'Oracle' FETCH FIRST ROW ONLY), 
    'Advanced',
    'Oracle',
    (SELECT (Client_ID) FROM Client WHERE Level = 'Advanced' AND Specialisation = 'Oracle' FETCH FIRST ROW ONLY),
    'Advanced',
    'Oracle'
    );
SELECT lives_ok(
    'correct_registration',
    'Accepting proper registration'
);

PREPARE registration_by_trainer AS UPDATE Registration SET Employee_ID = (SELECT (Employee_ID) FROM Employee WHERE position = 'Trainer' FETCH FIRST ROW ONLY);
SELECT throws_ok(
    'registration_by_trainer',
    '23503',
    'insert or update on table "registration" violates foreign key constraint "fk_registration_administrator"',
    'Not possible to be registered by trainer'
);

PREPARE registration_level_mismatch AS INSERT INTO Registration (Registration_Date, Attendees, Employee_ID, Event_ID, Event_Level, Event_Specialisation, Client_ID, Client_Level, Client_Specialisation) VALUES 
    ('March 30, 2015', 8,
    (SELECT (Employee_ID) FROM Employee WHERE position = 'Administrator' FETCH FIRST ROW ONLY),
    (SELECT (Event_ID) FROM Event WHERE Level = 'Advanced' FETCH FIRST ROW ONLY), 
    'Advanced',
    'Oracle',
    (SELECT (Client_ID) FROM Client WHERE Level = 'Beginner' FETCH FIRST ROW ONLY),
    'Beginner',
    'Oracle');
SELECT throws_ok(
    'registration_level_mismatch',
    '23514',
    'new row for relation "registration" violates check constraint "registration_level_match"',
    'Client and Event level mismatch'
);

PREPARE registration_spec_mismatch AS INSERT INTO Registration (Registration_Date, Attendees, Employee_ID, Event_ID, Event_Level, Event_Specialisation, Client_ID, Client_Level, Client_Specialisation) VALUES 
    ('March 30, 2015', 9,
    (SELECT (Employee_ID) FROM Employee WHERE position = 'Administrator' FETCH FIRST ROW ONLY),
    (SELECT (Event_ID) FROM Event WHERE Specialisation = 'Oracle' FETCH FIRST ROW ONLY),
    'Advanced',
    'Oracle',
    (SELECT (Client_ID) FROM Client WHERE Specialisation != 'Oracle' AND Level = 'Advanced' FETCH FIRST ROW ONLY),
    'Advanced',
    'Microsoft');
SELECT throws_ok(
    'registration_spec_mismatch',
    '23514',
    'new row for relation "registration" violates check constraint "registration_specialisation_match"',
    'Client and Event specialisation mismatch'
);

SELECT * FROM finish();
ROLLBACK;