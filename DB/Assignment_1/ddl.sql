DROP TABLE IF EXISTS Registration CASCADE;
DROP TABLE IF EXISTS Client CASCADE;
DROP TABLE IF EXISTS Event_Session CASCADE;
DROP TABLE IF EXISTS Event CASCADE;
DROP TABLE IF EXISTS Trainer CASCADE;
DROP TABLE IF EXISTS Administrator CASCADE;
DROP TABLE IF EXISTS Employee CASCADE;

CREATE TABLE Employee(
    Employee_ID serial 
        CONSTRAINT PK_Employee PRIMARY KEY,
    Surname char(64) NOT NULL,
    Forename char(64) NOT NULL,
    Start_Date date NOT NULL,
    Manager int 
        CONSTRAINT FK_Employee REFERENCES Employee(Employee_ID),
    Position char(16) NOT NULL 
        CONSTRAINT Employee_Position CHECK(Position IN ('Trainer', 'Administrator')),
    CONSTRAINT Unique_Employee_ID_Position UNIQUE (Employee_ID, Position)
);

CREATE TABLE Trainer(
    Employee_ID int
        CONSTRAINT PK_Trainer PRIMARY KEY 
        CONSTRAINT FK1_Trainer_Employee REFERENCES Employee(Employee_ID),
    Position char(16) NOT NULL
        CONSTRAINT Employee_Position CHECK(Position IN ('Trainer', 'Administrator')),
    Certification char(16) NOT NULL 
        CONSTRAINT Trainer_Certification CHECK(Certification IN ('OCP', 'MCP')),
    Level char(16) NOT NULL 
        CONSTRAINT Trainer_Level CHECK(Level IN ('Trainer', 'Consultant')),
    CONSTRAINT FK2_Trainer_Employee FOREIGN KEY (Employee_ID, Position) REFERENCES Employee(Employee_ID, Position),
    CONSTRAINT Unique_Employee_ID_Certification UNIQUE (Employee_ID, Certification),
    CONSTRAINT Unique_Employee_ID_Level UNIQUE (Employee_ID, Level)
);

CREATE TABLE Administrator(
    Employee_ID int
        CONSTRAINT PK_Administrator PRIMARY KEY 
        CONSTRAINT FK1_Administrator_Employee REFERENCES Employee(Employee_ID),
    Position char(16) NOT NULL
        CONSTRAINT Employee_Position CHECK(Position IN ('Trainer', 'Administrator')),
    Bonus boolean NOT NULL,
    CONSTRAINT FK2_Administrator_Employee FOREIGN KEY (Employee_ID, Position) REFERENCES Employee(Employee_ID, Position)
);

CREATE TABLE Event(
    Event_ID serial 
        CONSTRAINT PK_Event PRIMARY KEY,
    Event_Name char(256) NOT NULL,
    Event_Date date NOT NULL,
    Seats_Availible int NOT NULL,
    Event_Cost decimal(10,2) NOT NULL,
    Specialisation char(32) NOT NULL 
        CONSTRAINT Event_Specialisation CHECK(Specialisation IN ('Oracle', 'Microsoft')),
    Level char(16) NOT NULL 
        CONSTRAINT Event_Level CHECK(Level IN ('Beginner', 'Intermediate', 'Advanced')),
    CONSTRAINT Unique_Event_ID_Level UNIQUE (Event_ID, Level),
    CONSTRAINT Unique_Event_ID_Specialisation UNIQUE (Event_ID, Specialisation)
);

CREATE TABLE Event_Session(
    Session_ID serial 
        CONSTRAINT PK_Event_Session PRIMARY KEY,
    Session_Number int NOT NULL,
    Employee_ID int NOT NULL
        CONSTRAINT FK_Event_Session_Trainer REFERENCES Trainer(Employee_ID),
    Event_ID int NOT NULL
        CONSTRAINT FK_Event_Session_Event REFERENCES Event(Event_ID),
    Event_Specialisation char(32) NOT NULL 
        CONSTRAINT Event_Specialisation CHECK(Event_Specialisation IN ('Oracle', 'Microsoft')),
    Event_Level char(16) NOT NULL 
        CONSTRAINT Event_Level CHECK(Event_Level IN ('Beginner', 'Intermediate', 'Advanced')),
    Trainer_Certification char(16) NOT NULL 
        CONSTRAINT Trainer_Certification CHECK(Trainer_Certification IN ('OCP', 'MCP')),
    Trainer_Level char(16) NOT NULL 
        CONSTRAINT Trainer_Level CHECK(Trainer_Level IN ('Trainer', 'Consultant')),
    CONSTRAINT FK_Event_ID_and_Level FOREIGN KEY (Event_ID, Event_Level) REFERENCES Event(Event_ID, Level),
    CONSTRAINT FK_Event_ID_and_Specialisation FOREIGN KEY (Event_ID, Event_Specialisation) REFERENCES Event(Event_ID, Specialisation),
    CONSTRAINT FK_Employee_ID_and_Specialisation FOREIGN KEY (Employee_ID, Trainer_Certification) REFERENCES Trainer(Employee_ID, Certification),
    CONSTRAINT FK_Employee_ID_and_Level FOREIGN KEY (Employee_ID, Trainer_Level) REFERENCES Trainer(Employee_ID, Level),
    CONSTRAINT Event_Session_Specialisation_Match Check (
        (Event_Specialisation = 'Microsoft' AND Trainer_Certification = 'MCP')
        OR (Event_Specialisation = 'Oracle' AND Trainer_Certification = 'OCP')),
    CONSTRAINT Event_Session_Level_Match Check (
        Event_Level != 'Advanced' 
        OR (Event_Level = 'Advanced' AND Trainer_Level = 'Consultant'))
);

CREATE TABLE Client(
    Client_ID serial 
        CONSTRAINT PK_Client PRIMARY KEY,
    Surname char(64) NOT NULL,
    Forename char(64) NOT NULL,
    Street char(256) NOT NULL,
    Suburb char(64) NOT NULL,
    City char(64) NOT NULL,
    Country char(64) NOT NULL,
    Post_Code char(64) NOT NULL,
    Phone char(64) NOT NULL,
    Business_Name char(256),
    Employee_Total int,
    Specialisation char(32) NOT NULL 
        CONSTRAINT Event_Specialisation CHECK(Specialisation IN ('Oracle', 'Microsoft')),
    Level char(16) NOT NULL 
        CONSTRAINT Event_Level CHECK(Level IN ('Beginner', 'Intermediate', 'Advanced')),
    CONSTRAINT Unique_Client_ID_Level UNIQUE (Client_ID, Level),
    CONSTRAINT Unique_Client_ID_Specialisation UNIQUE (Client_ID, Specialisation)
);

CREATE TABLE Registration(
    Registration_ID serial 
        CONSTRAINT PK_Registration PRIMARY KEY,
    Registration_Date date NOT NULL,
    Attendees int NOT NULL,
    Employee_ID int NOT NULL
        CONSTRAINT FK_Registration_Administrator REFERENCES Administrator(Employee_ID),
    Event_ID int NOT NULL
        CONSTRAINT FK_Registration_Event REFERENCES Event(Event_ID),
    Event_Level char(16) NOT NULL 
        CONSTRAINT Registration_Event_Level CHECK(Event_Level IN ('Beginner', 'Intermediate', 'Advanced')),
    Event_Specialisation char(32) NOT NULL 
        CONSTRAINT Registration_Event_Specialisation CHECK(Event_Specialisation IN ('Oracle', 'Microsoft')),
    Client_ID int NOT NULL
        CONSTRAINT FK_Registration_Client REFERENCES Client(Client_ID),
    Client_Level char(16) NOT NULL 
        CONSTRAINT Registration_Client_Level CHECK(Client_Level IN ('Beginner', 'Intermediate', 'Advanced')),
    Client_Specialisation char(32) NOT NULL 
        CONSTRAINT Event_Specialisation CHECK(Client_Specialisation IN ('Oracle', 'Microsoft')),
    CONSTRAINT FK_Event_ID_and_Level FOREIGN KEY (Event_ID, Event_Level) REFERENCES Event(Event_ID, Level),
    CONSTRAINT FK_Event_ID_and_Specialisation FOREIGN KEY (Event_ID, Event_Specialisation) REFERENCES Event(Event_ID, Specialisation),
    CONSTRAINT FK_Client_ID_and_Level FOREIGN KEY (Client_ID, Client_Level) REFERENCES Client(Client_ID, Level),
    CONSTRAINT FK_Client_ID_and_Specialisation FOREIGN KEY (Client_ID, Client_Specialisation) REFERENCES Client(Client_ID, Specialisation),
    CONSTRAINT Registration_Level_Match Check (Event_Level = Client_Level),
    CONSTRAINT Registration_Specialisation_Match Check (Event_Specialisation = Client_Specialisation)
);