\documentclass[a4paper, jou, natbib, apacite]{apa6}
\usepackage[utf8]{inputenc}
\usepackage{enumitem}
% \usepackage{graphicx}

\usepackage{url}


\title{Agile Project Management Methods}
\shorttitle{Agile Methods}
\author{Dmitry Shiltsov}
\affiliation{Whitireia New Zealand}

\abstract{This paper briefly explains such Agile methodologies as Scrum, Kanban and Extreme Programming. These methodologies are compared and their applicability to the particular study case is evaluated.}
\keywords{Agile, Scrum, Kanban, Extreme programming}


\begin{document}

\maketitle


\section{Introduction}

\subsection{Problem Background}

In the last decade Agile methodology became very popular approach in software development. The epoch of Agile can be traced from the declaration of Agile Manifesto \cite{agilemanifesto} while most of methods and tools have been used long before that moment: iterative and incremental approaches, response to change, involvement of customers, software over documentation \cite{Abbas_Historical_roots_of_Agile_methods_where_did_Agile_thinking_come_from}.

For our study case we will investigate some Agile methodologies and apply them to a team of 5 programmers currently working on a mobile application using traditional waterfall approach.


\subsection{Research Methodology}

Being de facto a new standard in the software industry, Agile attracted a lot of attention from academical institutes. For this work we will start with academical articles and publications but also take into consideration all sources cited by these works. After defining each particular approach we will look for their similarities and differences. To make a conclusion we will discuss the applicability of each method in our case.




\section{Methodologies}






\subsection{Scrum}

\subsubsection{History}

The usage of term ``Scrum'' in software development is usually associated with the article ``The new new product development game'' \cite{Takeuchi_The_new_new_product_development_game}. In this work authors compared traditional (linear) approach with overlap ``rugby'' approach: ``Under the rugby approach, the product development process emerges from the constant interaction of a hand-picked, multidisciplinary team whose members work together from start to finish. Rather than moving in defined, highly structured stages, the process is born out of the team members' interplay''. This was illustrated by Figure~.\ref{fig:rugby_approach}.

\begin{figure}[h]
    \fitfigure[]{rugby_approach.pdf}
    \caption{\label{fig:rugby_approach} Sequential (A) vs. overlapping (B and C) phases of development.} 
\end{figure}

Later the word ``Scrum'' was used by \citeA{sutherland2004agile} in the article ``Agile development: Lessons learned from the first scrum''. According to \cite {Rising_The_Scrum_software_development_process_for_small_teams}, most of the Scrum elements were not new at that moment, for example some of them came from Barry Boehm’s spiral model. Finally the Scrum Alliance was established and the number of Certified Scrum Master programs were created. 

\subsubsection{Roles}

According to \citeA{Schwaber_Agile_project_management_with_Scrum} there are three main roles.


\paragraph{The Product Owner} 

The product owner is usually a key stakeholder of the project. He gets, prioritizes and updates the list of requirements, determines most important features of a product. Generally he is responsible for having vision of the whole system and sharing this vision with the rest of the team.

\paragraph{Scrum Master} 

Sometimes considered as a process manager Scrum Master is responsible for keeping scrum process going. He coaches the team, keeps obstacles away and ensures maximum efficiency through using Scrum rules and practices. 


\paragraph{The Team} 

Usually consisting 5 -- 9 members, the team is responsible for delivering required functionality. Often team consists of different functional members e.g. software engineers, architects, testers, designers. These people are not directly controlled by Scrum Master or Product Owner but are self-managing and self-organizing. The team is responsible for completing each iteration as a whole, which enforces ``we are all in this together'' feeling.


\subsubsection{Scrum Flow}

\paragraph{Product backlog}

Scrum project is started by Product Owner, who transforms overall vision of the system and business needs into set of functional and nonfunctional requirements. Prioritized these requirements form the initial Product Backlog. Later this backlog can be changed to reflect changes in business needs.

\paragraph{Sprints}

All work is done in Sprints, which usually take 1 -- 4 weeks each. This flow is illustrated by ~\ref{fig:scrum}.

\begin{figure}[h]
    \centering \includegraphics[width=\linewidth]{scrum-process1.png}
    \caption{\label{fig:scrum} Scrum work flow (from heliosobjects.com)} 
\end{figure}

\subparagraph{Planning} Each sprint starts with planning meeting (usually limited by reasonable amount of time). During this meeting Product Owner and the Team together set \emph{Sprint Backlog} by choosing most critical issues from Product Backlog. The point of this event is to choose as much required functionality as possible to do through the Sprint time. 

\subparagraph{Daily Scrums} This is short meeting taken each day, where each member of the team answers three questions:
\begin{itemize}[noitemsep]
    \item What has been done since the last daily Scrum?
    \item What are the plans till the next daily Scrum?
    \item Are there any obstacles to achieving the goals?
\end{itemize}

\subparagraph{Demo} During this meeting, which finishes the sprint, the Team presents to Product Owner results of the sprint, which helps their collaboration. It is assumed that these results should be ready for production, e.~g. code should be structured, tested and documented.


\subparagraph{Retrospective} Before starting next Sprint planning Scrum Master gathers the Team in order to commit ``Lessons learned'' session. Members are encouraged to improve processes and work-flows which can help during the next Sprints.







\subsection{Kanban}

\subsubsection{History}

The name 'Kanban' originates from Japanese, and translates roughly as "signboard" or "billboard". It was introduced by Toyota in the late 1940 which basically adopted processes of keeping supermarket stocks to manufacturing. In the world of software development it was largely popularized by \citeA{anderson2003agile} in his book ``Agile management for software engineering: Applying the theory of constraints for business results''. Another well known work describing underlying Kanban fundamentals is \citeA{Poppendieck_Lean_software_development_an_agile_toolkit}.

\subsubsection{Basic principles}

There is no strict set of roles or procedures. Initially existing organization is accepted by Kanban as is. After that company starts moving through continuous evolution of management style. While rapid changes might look more effective, they can be rejected and feared by people. Moreover some key elements of work, existing roles and job titles can be quite effective and should be preserved. But despite the titles acts of leadership are encouraged throughout the company including the lowest ranks. These principles are supported by the following practical approaches.
    
    
\subsubsection{Kanban Practices}

\paragraph{Visualize Work}

With using basic tools one can make work-flows more evident. This is especially useful in searching for unnecessary actions and bottlenecks. Proper visualization can help with communication and therefor collaboration.

\paragraph{Limit Work in Process}

One can increase effective performance of the whole system by limiting the total number of tasks in progress. Additional benefit comes from eliminating task switching and keeping them prioritized.

\paragraph{Manage Flow}

Team driven policies and minimal number of work in progress one can streamline the work-flow, invent appropriate metrics and discover potential sources of future problems.

\paragraph{Explicit policies}

It is impossible to discuss improvements of the process which is not explicit. Fundamental understanding will lead to more rational and objective arguing away from emotions and anecdotal proofs.

\paragraph{Continuous Improvement}

The culture of continuous improvement is a key feature of Kanban. Teams should carefully measure their effectiveness in order to improve it. Feedback loops provide additional informations on system inefficiency.








\subsection{Extreme programming}

While many its practices can be traced since 60s Extreme programming started to gain popularity since \cite{Beck_Extreme_programming_explained_embrace_change}. The name comes from the fact that all the practices came from best practices of software engineering pushed to extreme. For example such usual technique as code review when time before writing and reviewing approaches zero became pair programming. Test driven development (TDD) when tests are written before the code itself comes from traditional unit testing. There are four areas with 12 practices in total.

\subsubsection{Fine scale feedback}

\paragraph{Pair programming}
Two people are programming on the one machine -- the first one is directly typing in code, the other reviews it.

\paragraph{Planning game}
Methodology to evaluate tasks according its importance, risk and costs.

\paragraph{Test driven development}
Writing tests before the code itself.

\paragraph{Whole team}
Having an experienced user of future product in the team.


\subsubsection{Continuous process}

\paragraph{Continuous integration}
The team is working with the latest version of code. Each change is merged with trunk as soon as possible.

\paragraph{Refactoring or design improvement}
Making the code easier to understand and maintain changing architecture when necessary. 

\paragraph{Small releases}
Frequent releases with concrete value.

\subsubsection{Shared understanding}

\paragraph{Coding standards}
Standards on style and format for the code are shared by the whole team.

\paragraph{Collective code ownership}
Everybody is allowed to fix any part of the code.

\paragraph{Simple design}
Keeping the system as simple as possible.

\paragraph{System metaphor}
Naming each part of the system the way everyone can guess its task.

\subsubsection{Programmer welfare}

\paragraph{Sustainable pace}
Keeping programmers' overtime as low as possible.




\subsection{Similarities and differences}

\subsubsection{Literature review}

In the literature relation between different Agile approaches differs from direct competition to full complementation and mutual support. 

\citeA{Sjoberg_Quantifying_the_effect_of_using_kanban_versus_scrum_A_case_study} report about Scrum being replaced by Kanban and even compares change in key performance indicators during this process. 

While accepting contradiction between Kanban and ``typical Agile adoption'' (Scrum), \citeA{Polk_Agile_and_Kanban_in_Coordination} state that blending them together can add a significant improvement to efficiency. 


\citeA{Mahnic_Improving_Software_Development_through_Combination_of_Scrum_and_Kanban} come to conclusion, that Scrum and Kanban are not mutually exclusive competing methodologies, but can complement each other to facilitate higher performance in software development teams. The former provides a framework for iterative and incremental development process, whereas the latter ensures high visibility of the workflow and quick identification of possible bottlenecks, thus enabling continuous process improvement.

\citeA{Bashir_Hybrid_Software_Development_Approach_For_Small_To_Medium_Scale_Projects_Rup_Xp_Scrum} highlight mutual support of Scrum and Extreme programming in case of small and medium projects.

\subsubsection{Direct comparison}

``Kanban and Scrum -- making the most of both'', the book by \citeA{kniberg2010kanban}, compares agile tools by the measure of ``
``prescriptiveness''. With estimated number of practices for Extreme Programming being 13, Scrum -- 9 and Kanban -- 3 the book concludes that Kanban is the most adaptive approach. But it warns against choosing the tool by this measure and strongly suggests picking more than one. According to the book list of Scrum and Kanban similarities looks this way:

\begin{itemize}[noitemsep]
 \item Both are Lean and Agile
 \item Both use pull scheduling
 \item Both limit WIP
 \item Both use transparency to drive process improvement
 \item Both focus on delivering releasable software early and often
 \item Both are based on self-organizing teams
 \item Both require breaking the work into pieces
 \item In both, release plan is continuously optimized based on empirical data (velocity / lead time)
\end{itemize}

Table~\ref{table:diff} \cite{kniberg2010kanban} illustrates differences between Scrum and Kanban.

\begin{table*}[p]
    \fitfigure[]{scrum_vs_kanban.pdf}
    \caption{\label{table:diff} Differences between Scrum and Kanban} 
\end{table*}

One of Kanban principles is constant process evolution particularly via providing feedback. Figure~\ref{fig:feedback_loops} illustrates combined feedback loops of Scrum and Extreme programming techniques. 

\begin{figure}[h]
    \centering \includegraphics[width=0.8\linewidth]{DonWells_Extreme_Programming.png}
    \caption{\label{fig:feedback_loops} Agile feedback loops} 
\end{figure}


\subsubsection{Appropriate uses}

As explained in the previous sections of this work Scrum, Kanban and Extreme programming can be applied to the company of any size. In the same time Scrum and Extreme programming are intended to be used by teams, while Kanban can be applied to the organization as a whole. All these approaches fit nicely with each other or can be adopted partially.


\section{Recommendation}

As defined in our case study we have salespeople with some ideas who will most likely come up with the new ones during the process of application development. Given that, lack of Agile experience and the small size of our team we can immediately suggest Scrum methodology for this particular reasons:
\begin{itemize}[noitemsep]
    \item Well defined and easy to follow procedures.
    \item Great collaboration with product users.
    \item Good control over the project scope.
\end{itemize}

Extreme Programming can take some time to master, but due to continuous process will allow us to change functionality of application in more fast and reliable way. As our team is only 5 people size, other benefits of this methodology (like shared understanding) might not be that important.

Kanban is attractive because it doesn't require a lot of changes in the work-flow to start with and aims to solve all our problems with the scope and user involvement. But being the most adaptable methodology it is the most difficult one to master.
    

\bibliography{Agile_Report}

\end{document}
