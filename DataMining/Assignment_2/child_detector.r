#!/usr/bin/env Rscript
# .libPaths("/home/shield/R/x86_64-pc-linux-gnu-library/3.1")


#############      Getting data from DB       ###############

library(RODBC)
s <- odbcDriverConnect("dsn=sales")
customers <- sqlQuery(s, "select Homeowner, MaritalStatus, NumCarsOwned, NumChildrenAtHome from customer where PK_Customer>=5000", stringsAsFactors=TRUE)

customers$Homeowner <- customers$Homeowner == 'Y'
customers$MaritalStatus <- customers$MaritalStatus == 'Y'

#We don't care about number of children:
customers$ChildrenAtHome <- as.factor(customers$NumChildrenAtHome > 0)



#############      Splitting data to train and test sets       ###############

library(caret)

#Splitting set into training and testing ones
set.seed(777)
inTrain <- createDataPartition(y=customers$ChildrenAtHome, p=0.5, list=FALSE)
customers_training <- customers[inTrain, ]
customers_testing <- customers[-inTrain, ]



#############       Decision tree        ###############

library(rpart)


modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="rpart")

#Showing results
print(modelFit$finalModel)
# n= 4938 
# 
# node), split, n, loss, yval, (yprob)
#       * denotes terminal node
# 
#  1) root 4938 1032 TRUE (0.20899149 0.79100851)  
#    2) NumCarsOwned< 0.5 1479  695 FALSE (0.53008790 0.46991210)  
#      4) MaritalStatusTRUE< 0.5 637  240 FALSE (0.62323391 0.37676609)  
#        8) HomeownerTRUE< 0.5 421   24 FALSE (0.94299287 0.05700713) *
#        9) HomeownerTRUE>=0.5 216    0 TRUE (0.00000000 1.00000000) *
#      5) MaritalStatusTRUE>=0.5 842  387 TRUE (0.45961995 0.54038005)  
#       10) HomeownerTRUE>=0.5 423   36 FALSE (0.91489362 0.08510638) *
#       11) HomeownerTRUE< 0.5 419    0 TRUE (0.00000000 1.00000000) *
#    3) NumCarsOwned>=0.5 3459  248 TRUE (0.07169702 0.92830298) *




plot(modelFit$finalModel, uniform=TRUE, main="Children Classification Tree")
text(modelFit$finalModel)

#Testing model
predictions <- predict(modelFit, newdata=customers_testing)
confusionMatrix(predictions, customers_testing$ChildrenAtHome)


# Confusion Matrix and Statistics
# 
#           Reference
# Prediction FALSE TRUE
#      FALSE   814   59
#      TRUE    218 3846
#                                           
#                Accuracy : 0.9439          
#                  95% CI : (0.9371, 0.9501)
#     No Information Rate : 0.791           
#     P-Value [Acc > NIR] : < 2.2e-16       
#                                           
#                   Kappa : 0.8201          
#  Mcnemar's Test P-Value : < 2.2e-16       
#                                           
#             Sensitivity : 0.7888          
#             Specificity : 0.9849          
#          Pos Pred Value : 0.9324          
#          Neg Pred Value : 0.9464          
#              Prevalence : 0.2090          
#          Detection Rate : 0.1649          
#    Detection Prevalence : 0.1768          
#       Balanced Accuracy : 0.8868          
#                                           
#        'Positive' Class : FALSE 





############          Naive Bayes           ############

library(klaR)

modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="nb")

#Testing
predictions <- predict(modelFit, newdata=customers_testing)
confusionMatrix(predictions, customers_testing$ChildrenAtHome)

# Confusion Matrix and Statistics
# 
#           Reference
# Prediction FALSE TRUE
#      FALSE   414  212
#      TRUE    618 3693
#                                           
#                Accuracy : 0.8319          
#                  95% CI : (0.8212, 0.8422)
#     No Information Rate : 0.791           
#     P-Value [Acc > NIR] : 2.312e-13       
#                                           
#                   Kappa : 0.4056          
#  Mcnemar's Test P-Value : < 2.2e-16       
#                                           
#             Sensitivity : 0.40116         
#             Specificity : 0.94571         
#          Pos Pred Value : 0.66134         
#          Neg Pred Value : 0.85665         
#              Prevalence : 0.20903         
#          Detection Rate : 0.08386         
#    Detection Prevalence : 0.12680         
#       Balanced Accuracy : 0.67344         
#                                           
#        'Positive' Class : FALSE 




#############        Neural Network         ################

library(nnet)

modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="nnet")

print(modelFit$finalModel)
# a 3-5-1 network with 26 weights
# inputs: HomeownerTRUE MaritalStatusTRUE NumCarsOwned 
# output(s): .outcome 
# options were - entropy fitting  decay=0.1

predictions <- predict(modelFit, newdata=customers_testing)
confusionMatrix(predictions, customers_testing$ChildrenAtHome)

# Confusion Matrix and Statistics
# 
#           Reference
# Prediction FALSE TRUE
#      FALSE   814   59
#      TRUE    218 3846
#                                           
#                Accuracy : 0.9439          
#                  95% CI : (0.9371, 0.9501)
#     No Information Rate : 0.791           
#     P-Value [Acc > NIR] : < 2.2e-16       
#                                           
#                   Kappa : 0.8201          
#  Mcnemar's Test P-Value : < 2.2e-16       
#                                           
#             Sensitivity : 0.7888          
#             Specificity : 0.9849          
#          Pos Pred Value : 0.9324          
#          Neg Pred Value : 0.9464          
#              Prevalence : 0.2090          
#          Detection Rate : 0.1649          
#    Detection Prevalence : 0.1768          
#       Balanced Accuracy : 0.8868          
#                                           
#        'Positive' Class : FALSE   



#############        Neural Network (MLP)        ################

library(RSNNS)
modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="mlp")

caret::confusionMatrix(predictions, customers_testing$ChildrenAtHome)

# Confusion Matrix and Statistics
# 
#           Reference
# Prediction FALSE TRUE
#      FALSE   814   59
#      TRUE    218 3846
#                                           
#                Accuracy : 0.9439          
#                  95% CI : (0.9371, 0.9501)
#     No Information Rate : 0.791           
#     P-Value [Acc > NIR] : < 2.2e-16       
#                                           
#                   Kappa : 0.8201          
#  Mcnemar's Test P-Value : < 2.2e-16       
#                                           
#             Sensitivity : 0.7888          
#             Specificity : 0.9849          
#          Pos Pred Value : 0.9324          
#          Neg Pred Value : 0.9464          
#              Prevalence : 0.2090          
#          Detection Rate : 0.1649          
#    Detection Prevalence : 0.1768          
#       Balanced Accuracy : 0.8868          
#                                           
#        'Positive' Class : FALSE   


#############     Comparison    ###################

modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="rpart")
rpart_predictions <- predict(modelFit, newdata=customers_testing)

modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="nnet")
nnet_predictions <- predict(modelFit, newdata=customers_testing)

modelFit <- train(ChildrenAtHome ~ Homeowner+MaritalStatus+NumCarsOwned, data=customers_training, method="mlp")
mlp_prediction  <- predict(modelFit, newdata=customers_testing)



confusionMatrix(nnet_predictions, mlp_prediction)
# Confusion Matrix and Statistics
# 
#           Reference
# Prediction FALSE TRUE
#      FALSE   857    0
#      TRUE      0 4080

confusionMatrix(nnet_predictions, rpart_predictions)
# Confusion Matrix and Statistics
# 
#           Reference
# Prediction FALSE TRUE
#      FALSE   857    0
#      TRUE      0 4080



#############     Clustering      ###################
library(cluster)
training_dist <- daisy(customers_training[,c("Homeowner","MaritalStatus","NumCarsOwned")])


training_dist <- daisy(customers_training[sample(1:nrow(customers_training),150,replace=FALSE),c("Homeowner","MaritalStatus","NumCarsOwned")])
hc<-hclust(training_dist)
hc$labels<-customers_training[hc$labels,"ChildrenAtHome"]
plot(hc)
#There are 2 apparent 'FALSE' (no children) clusters and 3 ~ 6 TRUE (children at home) clusters.

