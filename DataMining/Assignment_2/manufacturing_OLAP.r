#!/usr/bin/env Rscript
.libPaths("/home/shield/R/x86_64-pc-linux-gnu-library/3.1")

#############      Getting data from DB       ###############

library(RODBC)
s <- odbcDriverConnect("dsn=manufacturing")


# Loading all data from DB:
manufacturing <- sqlQuery(s,"select 
    AcceptedProducts, RejectedProducts, BatchName, ElapsedTimeForManufacture,
    ProductName, ProductSubtypeName, ProductTypeName,
    MachineName, dimmachinetype.MachineType, dimmaterial.Material,
                 PlantName, CountryName, Manufacturer, DateOfPurchase,
                 Date_Name, Month_Name, Quarter_Of_Year_Name, Year_Name
    
    from manufacturingfact  
        join dimbatch on dimbatch.BatchNumber = manufacturingfact.BatchNumber
        join dimproduct on dimproduct.ProductCode = manufacturingfact.ProductCode 
        join dimproductsubtype on dimproductsubtype.ProductSubtypeCode = dimproduct.ProductSubtypeCode 
        join dimproducttype on dimproducttype.ProductTypeCode = dimproductsubtype.ProductTypeCode 
        join dimmachine on dimmachine.MachineNumber = manufacturingfact.MachineNumber
        join dimmachinetype on dimmachinetype.MachineType =  dimmachine.MachineType
        join dimmaterial on dimmaterial.Material = dimmachinetype.Material
        join dimplant on dimplant.PlantNumber = dimmachine.PlantNumber
        join dimcountry on dimcountry.CountryCode = dimplant.CountryCode
        join time on time.PK_Date = manufacturingfact.DateOfManufacture;", stringsAsFactors=TRUE)

        
# Adding RejectedPercentage
manufacturing$RejectedPercentage = manufacturing$RejectedProducts/(manufacturing$RejectedProducts + manufacturing$AcceptedProducts) * 100





################ Building cube ##########################

# Requirements:

# What: AcceptedProducts, RejectedProducts, RejectedPercentage, ElapsedTimeForManufacture

# By: c("BatchName", , "ProductName", "ProductSubtypeName", "ProductTypeName", "MachineName", "MachineType", "Material", "PlantName", "CountryName", "Manufacturer", "DateOfPurchase", "Date_Name", "Month_Name", "Quarter_Of_Year_Name", "Year_Name")



mysum <- function(x) {return(sum(x, na.rm=TRUE))}

# Naive attempt to build a full cube fails because enormous size of multidimensional array. For proof of concept we will satisfy only part of requirements

#Starting with small cubes

AcceptedProducts_cube <- tapply(manufacturing$AcceptedProducts, manufacturing[, c("ProductName", "Material", "Year_Name","MachineName")], mysum)

RejectedProducts_cube <- tapply(manufacturing$RejectedProducts, manufacturing[, c("ProductName", "Material", "Year_Name","MachineName")], mysum)

RejectedPercentage_cube <- tapply(manufacturing$RejectedPercentage, manufacturing[, c("ProductName", "Material", "Year_Name","MachineName")], mysum)


############ Combining in one big cube #################

cube <- array(c(AcceptedProducts_cube, RejectedProducts_cube, RejectedPercentage_cube),
    dim=c(dim(AcceptedProducts_cube),3), dimnames=append(dimnames(AcceptedProducts_cube),
    list(data=c("AcceptedProducts","RejectedProducts", "RejectedPercentage"))))

dimnames(cube)
# $ProductName
#  [1] "American GI"                  "Bear and Cub"                
# ...           
# [47] "US Navy Gunner's Mate"        "Wellington"                  
# [49] "Wolf Paperweight"             "Woodland Elf"                
# 
# $Material
# [1] "Aluminum" "Clay"     "Pewter"  
# 
# $Year_Name
# [1] "Calendar 2009" "Calendar 2010" "Calendar 2011"
# 
# $MachineName
# [1] "BlandaCast 900" "Duramolder"     "FineMold 1000"  "FineMold 1010" 
# [5] "PrecisionCraft"
# 
# $data
# [1] "AcceptedProducts"   "RejectedProducts"   "RejectedPercentage"




############ Using cube #################

####### "Slicing" (fixing some dimensions) ########

# Taking only AcceptedProducts in 2009
cube[,,"Calendar 2009",,"AcceptedProducts"]
# ... 
# , , MachineName = FineMold 1010
# 
#                               Material
# ProductName                    Aluminum Clay Pewter
#   American GI                      7304   NA     NA
#   Bear and Cub                       NA   NA     NA
# ...


cube[,"Pewter","Calendar 2009","PrecisionCraft","AcceptedProducts"]
#                  American GI                 Bear and Cub 
#                           NA                           NA 
#               Bear with Hive Boston, MS Harbor Lighthouse 
#                           NA                           NA 
#              British Cavalry             British Infantry 
#                           NA                           NA 
#       British Tank Commander                   Bull Moose 
#                           NA                           NA 
# Cape Hatteras, NC Lighthouse       Chicago, IL Lighthouse 
#                           NA                           NA 
#           Dragon with Knight                      Drummer 
#                       177012                           NA 
#                   Eagle Pair            Eagle Paperweight 
#                           NA                           NA 
#         Eddistone Lighthouse                  Elvin Dance 
#                           NA                       212648 
#                 Female Troll                Flying Dragon 
#                        63397                       256116 
# ...
#                           NA                           NA 
#             Russian Infantry               Serpent Dragon 
#                           NA                        91291 
#                Soaring Eagle    Split Rock, MN Lighthouse 
#                           NA                           NA 
#                  Troll Party                US Army Pilot 
#                        64169                           NA 
#        US Navy Gunner's Mate                   Wellington 
#                           NA                           NA 
#             Wolf Paperweight                 Woodland Elf 
#                           NA                         5368 


####### "Dicing" (fixing some values) ########

# Taking particular products, materials and only accepted number
cube[c("Serpent Dragon","Troll Party","Moose with Calf"),c("Pewter","Clay"),,,"AcceptedProducts"]

#...
# , , Year_Name = Calendar 2009, MachineName = Duramolder
# 
#                  Material
# ProductName       Pewter  Clay
#   Serpent Dragon      NA    NA
#   Troll Party         NA    NA
#   Moose with Calf     NA 89384
#...
#

######## "Rolling Up" (aggregating dimensions) #########

# Summirizing per year. Deliberately left bug with percentage :) Shouldn't mix that in first place
apply(cube, c("Year_Name", "data"), mysum)
#                data
# Year_Name       AcceptedProducts RejectedProducts RejectedPercentage
#   Calendar 2009          3349380            35531           1529.228
#   Calendar 2010          3420133            36280           1537.919
#   Calendar 2011          3418942            36311           1537.212


########## "Drilling Down" (getting finer level of granularity)  ##########

# Mmm... kind of splitting meterials into products per each year
apply(cube[,,,,"AcceptedProducts"], c("ProductName", "Material", "Year_Name"), mysum)
#                               Material
# ProductName                    Aluminum   Clay Pewter
#   American GI                     41743      0      0
#   Bear and Cub                     2068 623645      0
#   Bear with Hive                      0  34217      0
#   Boston, MS Harbor Lighthouse        0 388445      0


###########  "Pivot" (taking pair of dimensions) ##################

#Accepted products per year
apply(cube[,,,,"AcceptedProducts"], c("ProductName", "Year_Name"), mysum)
#                               Year_Name
# ProductName                    Calendar 2009 Calendar 2010 Calendar 2011
#   American GI                          22483          8580         10680
#   Bear and Cub                        200249        226327        199137
# ...




#################  Combibations  ##################

# Checking accepted, rejected and percentage for Troll Party and Russian Infantry through years:
apply(cube[c("Troll Party","Russian Infantry"),,,,],c("Year_Name", "ProductName","data"), mysum)

# , , data = AcceptedProducts
# 
#                ProductName
# Year_Name       Troll Party Russian Infantry
#   Calendar 2009       64169            47571
#   Calendar 2010       33906            43567
#   Calendar 2011       48526            52052
# 
# , , data = RejectedProducts
# 
#                ProductName
# Year_Name       Troll Party Russian Infantry
#   Calendar 2009         687              498
#   Calendar 2010         362              455
#   Calendar 2011         521              547
# 
# , , data = RejectedPercentage
# 
#                ProductName
# Year_Name       Troll Party Russian Infantry
#   Calendar 2009   17.900205         35.14926
#   Calendar 2010    9.957039         31.36379
#   Calendar 2011   11.674392         34.60539


