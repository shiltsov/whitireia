\documentclass[a4paper,jou,natbib,apacite]{apa6} 
\usepackage[utf8]{inputenc}
\usepackage{apacite}
\usepackage{enumitem}

\title{Big Data tools and techniques}
\shorttitle{Big Data}

\author{Dmitry Shiltsov}
\affiliation{Whitireia New Zealand}


\abstract{This article introduces the rise of Big Data in the area of genome sequencing. The Hadoop ecosystem is reviewed as a most popular solution in the bioinformatics field. Some approaches to using R in Big Data environment are given. The article finishes with a criticisms of Big Data approach.}

\keywords{Big Data, Apache Hadoop, MapReduce, Apache Spark, R, RHIPE, RABID, sample bias, sample error}

\begin{document}
\maketitle 


\section{Introduction}

\subsection{Genome sequencing}
Human Genome Project \cite{Sawicki_Human_genome_project} started in 1984 opened way to new area in bioinformatics. The aim of this international project was to determine the sequence of DNA pairs in human cells and identify or map them to corresponding functional and physical features of a human body. The problem challenging by itself was hampered by the technology of DNA sequencing. Because of inability of machines to read the whole DNA in one run it should be split into millions fragments, which later should be recombined back in order to get the whole picture. Even nowadays with modern sequencers at hand, the sheer amount of possible genetic variations leads to enormous quantities of data. But thanks to the recent advancements in ``Big Data'' area one can order relatively comprehend analysis of his or her DNA for a reasonable sum of money. This article will observe some approaches and tools for dealing with large data sets.

\subsection{Structure of report}

In the next part a definition and main characteristics of Big Data are introduced. We will review several articles in the area of bioinformatics to look for suggested solutions in dealing with large datasets. We will overview these solutions briefly and provide some critique of the Big Data phenomena.

\subsection{Big Data}

Big Data as a relatively modern term has a lot of definitions. We will use one from \cite{Snijders_Big_data_Big_gaps_of_knowledge_in_the_field_of_internet_science} which concentrates on a technical aspect of problem: ``data sets so large and complex that they become awkward to work with using standard statistical software''. This definition presumes that there is no strict border between regular and big data and in particular case depends on the hardware and algorithms available. \citeA{Kaisler_Big_data_Issues_and_challenges_moving_forward} give four distinctive features of Big Data (also known as three 'V'):
\begin{description}
    \item [Volume] means challenge in storing capabilities of the analytical system.
    \item [Variety] assumes difficulties in Extract-Transform-Load (ETL) part of the data processing.
    \item [Velocity] sets a number of requirements to performance of the whole system.
\end{description}

\citeA{Huddar_A_Survey_on_Big_Data_Analytical_Tools} mention another 'V' for this list:
\begin{description}
    \item [Veracity] is a degree of trust to the information to base decisions on.
\end{description}



\subsection{Applicability of traditional approaches}

Carefully sampling is the proved way to deal with large sets of data. The most apparent problem of this approach is the process to get the right sample to minimize inevitable sampling error. 

More reasons to switch from traditional approaches are given in the work of  \citeA{Cohen_MAD_skills_new_analysis_practices_for_big_data}. For example, authors tell that the process of extracting data from the large database is sometimes less efficient than doing computation in-place. They claim that it is possible to get performance boost by the orders of magnitude simply by running code in the database itself. Another limitation is that most statistical packages expect the data to fit in the available operational memory. Unavoidable sampling in such cases leads to loosing details on the tails of distributions. In the modern world of personal targeting and understanding even the smallest subgroups it is hardly acceptable choice. While the using of parallel storage and computation is more and more affordable now, most of statistical libraries don't provide parallel implementation but if they do, it is usually based on relatively simple MPI (message parsing interface) technology. This means lots of extra efforts to integrate this kind of solution with other tools.



\section{Big Data in genomics}

According to \citeA{Nordberg_BioPig_a_Hadoop_based_analytic_toolkit_for_large_scale_sequence_data} researchers from the bioinformatics field experienced an exponential growth of sequenced data. This made most of their tools obsolete due to the inability to scale with the data size. That is why authors introduced the ``BioPig'' sequence analysis toolkit as a probable solution for todays challenges. Using the Apache's Hadoop and the Pig data flow language they have successfully tested their system at the National Energy Research Scientific Computing Center and in the Amazon Elastic Compute Cloud and demonstrated several significant advantages over the traditional serial and MPI-based algorithms:
\begin{itemize}
 \item The development for parallel processing is much easier and faster.
 \item The solution is automatically scalable.
 \item BioPig is portable across many Hadoop infrastructures.
\end{itemize}

Another work in the genomics by \citeA{ODriscoll_Big_data_Hadoop_and_cloud_computing_in_genomics} described methods to deal with biology's big data sets. After short introduction into cloud computing they suggested the usage of the Apache Hadoop project which is already a quite popular choice in the bioinformatics community for distributed and parallelized data processing and analysis.

\citeA{Taylor_An_overview_of_the_Hadoop_MapReduce_HBase_framework_and_its_current_applications_in_bioinformatics} is another article devoted to the Hadoop infrastructure and its successful application to bioinformatics area.

More details on implementing DNA-related algorithms BLAST, GSEA and GRAMMAR to the Hadoop environment are described in the work of \citeA{Leo_Biodoop_bioinformatics_on_hadoop}.

It seems that in the area of open source Big Data solutions the Apache Hadoop is a standard de facto. That is why we will make a short overview of its architecture in the next section.




\section{Hadoop ecosystem}

The Hadoop consists of several layers. The Hadoop framework is the core of the whole system. Maintainers try to keep it lean, that is why the core is surrounded by a number of closely connected but separate projects under the Apache brand \cite{Venner_Pro_Hadoop}. Figure~\ref{fig:eco} displays the dependency of some modules. There is also a plenty of independent projects which are using some Apache products as a base, several of them will be reviewed later in this work.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{HadoopEcosystem.png}
    \caption{\label{fig:eco} Hadoop Ecosystem} 
\end{figure}    


\subsection{Hadoop framework}

The core of Apache Hadoop framework consists of 3 parts:
\begin{description}
 \item [Hadoop Common] contains libraries and utilities shared by all other Hadoop modules.
 \item [Hadoop Distributed File System (HDFS)] is a distributed file-system aimed to commodity machines therefore providing remarkable total reliability and high aggregate bandwidth across the cluster.
 \item [Hadoop YARN / MapReduce 2.0] is a resource-manager for clusters, which implements users' tasks scheduling via MapReduce approach patented by Google.
\end{description}

\begin{figure}[h]
    \includegraphics[width=\linewidth]{mapreduce.jpg}
    \caption{\label{fig:mapreduce} The MapReduce approach (by Vincent Granville, datasciencecentral.com)}
\end{figure}   

The MapReduce approach is illustrated by Figure~\ref{fig:mapreduce}. The initial task is being split (mapped) into multiple independent parts and then all the partial results are being aggregated (reduced) to a single result.


 

\subsection{Related Apache projects}  

\subsubsection{ZooKeeper}

The ZooKeeper is used to centralize control over the cluster. It helps with configuration, naming and synchronization of machines. This task is especially important with all the race conditions and complex connections of nonuniform cluster.

\subsubsection{Pig}

Pig platform consists of ``Pig Latin'' language and its compiler. The typical compiler's output is a set of Map-Reduce programs. The language is characterized by the following key features:
\begin{itemize}
    \item Easy coding and parallel execution of appropriate data analysis tasks.
    \item Automatic  optimization of tasks by the system.
    \item Extensibility.
\end{itemize}



\subsubsection{Hive} 
The Apache Hive is a data warehouse specialized in large datasets kept in distributed storages. Usually queries are executed using SQL-like language called HiveQL, but the ability of plugging user's custom mappers and reducers is also supported.

\subsubsection{Mahout} 

The project's goal is to build a scalable machine learning library. The notable methods already supported by version 0.9 are:
\begin{itemize}
    \item K-Means, Fuzzy K-Means clustering.
    \item Naive Bayes classifier.
    \item Random forest classifier.
    \item Logistic regression classifier.
    \item Latent Dirichlet Allocation.
\end{itemize}

\subsubsection{HBase}  

This is a non-relational, distributed database with Java Client API inspired by Google's BigTable. The most prominent features are:

\begin{itemize}
    \item Strongly consistent reads/writes.
    \item Automatic sharding (redistribution) over clusters.
    \item Automatic RegionServer failover.
    \item Hadoop/HDFS integration.
    \item MapReduce support.
\end{itemize}   

\subsubsection{Drill}  

Apache Drill is an open source, low latency SQL query engine for Hadoop and NoSQL. This is an open source replacement for Google's Dremel project. The tool can interact with HDFS, Hive or HBase.

\subsection{Notable extensions}  


\subsubsection{Cloudera Impala}
As \citeA{Avrilia_SQL_on_Hadoop_Full_Circle_Back_to_Shared_Nothing} explain, Cloudera Impala is one of the first systems providing SQL support to Hadoop. Being developed before the Apache Drill, Cloudera Impala became quite popular due to its outstanding performance.

\subsubsection{Ricardo}
While the R language is very popular in bioinformatics its main limitation is that all computations should be done in the main memory. Ricardo system presented by \citeA{Das_Ricardo_integrating_R_and_Hadoop} partially resolves that limitation by splitting operations between R environment and Hadoop environment trying to minimize the transfer of data across system boundaries. 

\subsubsection{RHIPE}
The work of \citeA{Guha_Large_Complex_Data_Divide_and_Recombine_D_R_with_RHIPE} extends the idea of combining Hadoop and R language with RHIPE system. The main difference from more traditional R and MPI combination is that with Hadoop's help RHIPE provides not only 
parallel computation but also access to distributed file system, fine tasks scheduling, fault tolerance and sharing of the cluster by multiple users.


\subsection{Apache Spark}

\cite{Zaharia_Spark_cluster_computing_with_working_sets} report that two types of tasks are not very efficient in Hadoop's MapReduce approach:

\begin{description}
 \item [Iterative jobs] are jobs that assume using the same dataset many times.
 \item [Interactive analytics] usually involves loading the same dataset several times.
\end{description}

While Hadoop can express each of this task as a MapReduce routine, reading data from disk several times causes significant performance degradation. 

Authors  propose a new framework called Spark that supports these types of activity while keeping the same level of scalability and fault tolerance as Hadoop.  This is achieved by RDDs -- resilient distributed datasets. Each RDD is read-only collection of objects distributed across the cluster. The main feature of this abstraction is that if any part of RDD is lost, it can be rebuild by using other parts. 

The article states, that in iterative tasks Spark is 10 times faster than Hadoop. It also provides sub-second response time for interactive querying of 40Gb dataset. In the same time Spark can use the infrastructure of Hadoop, allowing easy switch from one to another. 

The success of Spark solution is characterized for example by the fact, that Apache Mahout project has set a goal to completely move from Hadoop to Spark as a base.


\subsubsection{RABID}

While being outstandingly fast, Spark requires knowledge of Scala and Java languages to fully leverage its abilities. To mitigate this problem and save the researchers' time \cite{Lin_RABID_A_Distributed_Parallel_R_for_Large_Datasets} provided a familiar R interface to the Spark framework called R Analytics for BIg Data (RABID). 

Authors compared the performance of the tool with Hadoop and RHIPE and have found that results are very promising. Using Linear Regression (LR) calculation as an example they demonstrated the scalability of solution with increase of data size (Figure~\ref{fig:RABID_DATA}) and the number of nodes (Figure~\ref{fig:RABID_NODES}).

\begin{figure}[h]
%     \includegraphics[width=\linewidth]{RABID_DATA.jpg}
    \fitfigure[]{RABID_DATA.pdf}
    \caption{\label{fig:RABID_DATA} Runtime in seconds for LR over 8 iterations on different data sizes}
\end{figure}   

\begin{figure}[h]
    \fitfigure[]{RABID_NODES.pdf}
    \caption{\label{fig:RABID_NODES} Runtime in seconds for LR over 8 iterations on different cluster sizes}
\end{figure}   





\section{Criticism of the big data paradigm}

With ability to process even higher volumes of information the Big Data raises a number of concerns in society. Some of them like intentional and unintentional discrimination covered by the publication of \citeA{Barocas_Big_Data_s_Disparate_Impact} are hardly applicable to genetic research. But those connected to theoretical base are quite universal.

Problem of sampling is unfold in the article of \citeA{Harford_Big_data_are_we_making_a_big_mistake}. The work cites
professor Viktor Mayer-Schönberger of Oxford’s Internet Institute, co-author of term ``Big Data'', who defines a big data set as one where ``N = All''. In such case there is no room for sampling error, because there is no sampling at all. Unfortunately it is impossible to have all the data, according to the Patrick Wolfe, a computer scientist and professor of statistics at University College London. It means that sampling bias in the case of Big Data is only presumed to be solved. Another problem is that with increase of data size a number of spurious patterns vastly outnumber genuine discoveries.

The last idea is explained by the work of \citeA{Taleb_Beware_the_Big_Errors_of_Big_Data}. They claim: ``Modernity provides too many variables, but too little data per variable. So the spurious relationships grow much, much faster than real information.'' It means that the number of false information also raises alongside with data size.

Authors illustrate the problem with observational studies in medical research where statistical relationships are examined on the researcher's computer. Double-blind testing reveals that more than eight times out of ten results of such observations are spurious. With big data researchers have even more opportunities to cherry pick statistics that confirm good results and ditch the rest.

\section{Conclusion}

We observed a number of articles dealing with the problem of large data sets caused by progress in DNA sequencing technologies and rising demand in this area. We concluded that the Hadoop environment is very popular solution to these challenges due a number of reasons including maturity, scalability, flexibility, and license model. We briefly overviewed the Hadoop ecosystem and mentioned the most prominent improvement over MapReduce approach implemented by the Apache Spark project. We also had some look on the ability to use R as an interface to Big Data infrastructure notably RHIPE and RABID. While we omitted most ethical drawbacks concerned with Big Data, we included some critique on the methodology. Taking everything into consideration we can conclude, that while the modern tools quite adequately solve the problem of dealing with large datasets, Big Data is more an evolutionary rather than a revolutionary step over traditional ways of data processing. The new approach should not be treated as a silver bullet and the apparent benefits come with inevitable costs and risks.




\bibliography{big_data}
\end{document}
