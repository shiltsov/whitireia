#!/bin/bash

if [[ $(hostname) == servoskull ]]; then 
    PORT=10034; 
else 
    PORT=10035; 
fi
echo "**** TUNNEL TO STUDENTS BY PORT $PORT ****"
autossh -N -X -R $PORT:localhost:22 students
