\documentclass[a4paper,12pt,twocolumn]{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{apacite}
\usepackage{url}
\usepackage{enumitem} 
\usepackage{dblfloatfix}
\bibliographystyle{apacite}
\usepackage{titlesec} 
% \newcommand{\sectionbreak}{\clearpage}

% \usepackage{setspace}   %debug
% \doublespacing          %debug


\usepackage{/home/shield/Whitireia/whitireiatitle}
\title{Migration to Cloud Infrastructure: a Cost-benefit Analysis for two Real Cases}
\author{Dmitry Shiltsov}
\course{IT8412 -- Cloud Computing and ICT Infrastructure}
\studentid{21402582}


\begin{document}

\begin{titlepage}
    \whitireiatitle{Assignment 2}{}
\end{titlepage}

\onecolumn

\begin{abstract}
    Nowadays Cloud infrastructure became a very attractive solution mainly due to its low cost and flexibility. But for many companies empty-headed migration to the Cloud might introduce serious risks and significant loses. In this work we briefly discuss numerous factors essential for choosing Cloud infrastructure. Then we examine two real life cases: large international company, looking for a new server to host a project, and a small startup choosing its first IT infrastructure. In both cases we study the major factors, which impact the decision, and perform a cost-benefit analysis. In the final part we compare the main decisive factors for both companies.
\end{abstract}

\clearpage

\tableofcontents
\twocolumn



\section{Introduction}

\subsection {Major factors for Cloud consideration}
The work of \citeA{rashmi2012five} lists the following factors related to Cloud migration:

\paragraph{Existing investments in IT.} If the company already has a large infrastructure, moving to the Cloud will be less obvious choice, because of existing resources and increased complexity. This is usually the case for larger organizations. 

\paragraph {Costs.} Combined from initial investments and operational expenditures, costs play the major role in decision making. Most of the risks can be estimated as possible money losses and added to equation. Compared with traditional solutions, Clouds offer pay-per-use schemes, which lead to significant savings.

\paragraph{Data and application security.} This is the major stopping factor for Cloud migration, because with all other conditions equal, storing sensitive data using shared hardware is definitely more risky. In severe cases it might cause complete redesign of applications to migrate.

\paragraph{Regulations.} Laws might add serious limitations on the ways, organization store their data. This is especially true for New Zealand, because being a small country with scarce population, it is often overlooked by global companies and treated as part of Australia.

\paragraph{Provisioning.} The flexibility and time to deploy is another serious factor for choosing Clouds solutions.

\paragraph{Network and support.} If your infrastructure is accessible over the external network connection, this connection should be reliable enough with acceptable latency and bandwidth, and upgrading it might be costly.

\paragraph{IT skills.} To fully leverage all the Cloud services provided, one should have a set of skills slightly different from ``old style'' system administrator.

\paragraph{Service Level Agreements.} Careful reading of SLAs should highlight what existing risks are managed by Cloud provider (hardware failure, storage redundancy), and what risks are not (security threats).

\subsection {Plan of analysis}

In next sections we will make a ``feasibility study'' similarly to the work of \citeA{khajeh2010cloud}. We will describe a couple of real life cases, enumerate possible solutions and consider the factors mentioned above. After performed cost-benefit analysis, we will choose the optimal solution and compare it with the decisions made by managers in each real case.



\clearpage

\section{Kaspersky New Server Case}

\subsection{Problem Statement}

Kaspersky Lab is one of the leading companies in computer antivirus industry. Along with antivirus it also provides numerous computer security services, anti-spam being one of them. Anti-spam service is provided by collaborative efforts of several groups in the domain of Laboratory of Content Filtration. We will examine a problem with IT infrastructure, that arose in Heuristic Analysis Group. 

This groups consists of senior spam analysts, who create special kind of code, which controls the logic of products and constitutes to spam detection. The core tools for spam processing, used by the group, have been written in the group. Apart from that specific instruments, group uses such collaboration tools as MediaWiki (documentation) and FlySpray (bug and task tracking). The fact that many analysts work from their homes (as a part of 24x7 service) makes things slightly more complex. All the tools outside the Heuristic Analysis Group and all the servers are supported by the Infrastructure group which is also part of Laboratory of Content Filtration (as shown in Figure~\ref{infra_rules_relation}).  

Unfortunately, the members of the Heuristics Group were not satisfied by the response time and level of support provided by the Infrastructure group. After the series of conflicts they demanded a full access to the servers involved in their activity in order to deal with the issues by themselves. The manager of the Infrastructure group couldn't accept it and decided to provide spam analysts with a separate server.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{infrastructure_heuristics_relation.pdf}
    \caption{\label{infra_rules_relation} Relation of groups} 
\end{figure}

\subsection{Analysis of possible solutions}

Apart from ordering and setting up a new dedicated server, which should take around two quarters, there were two obvious ways to quickly get a server: one is to allocate a virtual machine on one of existing servers with Xen hypervisor, and the other one is to rent a virtual machine from the Cloud provider used by the department (namely Amazon Web Services). Let's apply the Cloud migration criteria enumerated in the previous part of this work.

\subsubsection{Private virtual machine}
\paragraph{Costs}
    
    \subparagraph{Administration and support.}
        The initial repartitioning of host server resources and following setup might take around two or three hours of infrastructure group administrator. After that the most of the services will be carried by the Heuristic Analysis group, which costs nothing from the Infrastructure point of view.
    
    \subparagraph{Licensing}
        With exception of Kaspersky products, all the software is free and open source.
    
    \subparagraph{Software Updates and patching.}
        This activity should be done by Heuristic Analysis group, resulting in zero costs again.
    
    \subparagraph{Hosting expenses.}
        The host machine will be running any way, so the hosting expenses will stay mostly the same. Taking into consideration the relatively low performance requirements of analysts' server, the final share of hosting expenses and amortization will be quite low. Let's estimate it as \$100 per month. 
        
        Making the virtual machine to be easily accessible from outside of the Kaspersky network (because many analysts work from their own homes) is crucial and may be achieved by several ways. One way involves setting VPN access for analysts to the internal network, which includes lots of negotiation with company's system administrators and will inevitably take a lot of time. The other way is to set a proper domain name and make all the required adjustments with firewall. This is another task for the Infrastructure group system administrator which might take around one or two hours.
    
    \subparagraph{Overall.}
        Each hour of system administrator's work costs company at least \$50 USD. Therefore the initial setup will take around \$150-\$250. And each month will cost around \$100.
        
\paragraph{Benefits}
        From the users' perspective the main advantage of setting the virtual machine on one of the private servers is the proximity to other company servers. It means fast speed (and some reduction of external traffic).
        
        Another benefit is that private data and code will stay inside the corporate machine (apart from the data accessed from analysts' home machines).

\paragraph{Risks}
    \subparagraph{Limited host server capacity.} While the initial demanded resources are relatively low (single core and 4-8 Gb of RAM will be sufficient), this can still result in performance degradation of other virtual machines, hosted on the same physical server. This risk is low and will cause just another few hours of group's administrator attention. 
    
    Another risk is that the initial resource estimation is wrong, and analysts will ask for more powerful setup. This risk is moderate and will cause significant efforts from the Infrastructure group.
    
    \subparagraph{Vulnerability in the private network.} Taking into account questionable administration skills, there is a possibility to introduce a vulnerable host inside the corporate network. While this is an unlikely event, the consequences are tremendous for the company.
    

\subsubsection{Rented virtual machine from Amazon}
    \paragraph{Costs}

        \subparagraph{Administration and support.}
        Thanks to already existing Amazon account and easy to use interface, the process of acquiring and setting the access will take around half an hour of manager's work (the manager of the Infrastructure group is an ex-administrator).
        
        \subparagraph{Licensing.}
        As with the private virtual machine, all the software is free and open source.

        \subparagraph{Hosting expenses.}
        The monthly cost of t2.medium (2 cores and 4Gb of RAM) with Linux is \$35 per month. With the 100 Gb of the outgoing traffic the final monthly cost will stay inside \$70.
        
        \subparagraph{Overall.}
        The initial set-up will roughly cost \$25 USD.
        
        And each month will cost less than \$70.
    
\paragraph{Benefits}

        \subparagraph {Fast deployment.}
        This solution will be available to users in no time and will be done by the group manager himself.
        
        \subparagraph{Independence from the rest of infrastructure.}
        No resources from other projects are being involved. If the idea of dedicated server for senior analysts will fail, it will be easy to get rid of.
        
        \subparagraph{Performance flexibility.}
        During the early setup steps, the level of the instance to acquire can be changed really quickly without significant changes in cost. And during later steps, the project might be easily relocated to a new instance.
        
\paragraph{Risks}

        \subparagraph{Responsibility.} Most risks of the Cloud solution would be managed by the Heuristic Analysis group itself, and therefore might be ignored by Infrastructure group manager.
        
        \subparagraph{Security.} There is a possibility, that sensitive information about some company's know-hows will be located on the virtual machine. But taking into account the selected stack of technologies based on Linux even the questionable level of system administration should make the risk of leakage relatively low.

\subsection{Comparison and decision }

    \begin{table}[htb]
        \begin{tabular}{r|c|c|c}
            ~              & Costs     & Benefits  & Risks  \\ \hline
            Local virtual  & Low       & Low       & High   \\
            Remote virtual & Low       & Medium    & Low \\
        \end{tabular} 
        \caption{Comparison for Kaspersky case.}
    \end{table}

    On the one hand we have all the data being kept in-house and better network connectivity, but unacceptable network vulnerability. 
    
    On the other hand we have better cost and flexibility, but recognizable risk of the leakage. With no doubts the infrastructure manager has chosen the Cloud solution.

\clearpage
\section{Regen Infrastructure Case}

\subsection{Problem Statement}

After successful proof of concept, Harmonics, the company who specialises in data analysis and modelling, established an independent startup company called Regen. The business of Regen is about taking data from sensors installed on farms, process it and send results to the farm owners. 

While being part of Harmonics, the project was hosted on Harmonics' servers right in the office, but after separation this couldn't stay like that any longer. One of the first steps towards the independence was to get its own infrastructure.  The initial design involved at least two machines: production and preproduction servers. In the future, depending on the success of the business (the total load of new services and the number of clients), database and frontend might be separated from backend.

\subsection{Analysis of possible solutions}
There are several obvious ways to get the hardware required:
\begin{itemize}
    \item Buy servers and place them in the data centre.
    \item Rent already installed servers.
    \item Rent virtual instances.
\end{itemize}

\subsubsection{Buying servers}
    \paragraph{Costs}
        A couple of acceptable servers will cost around \$10000 NZD. The help of external system administrator to fully setup machines will be around \$500. Monthly cost for each of 2U server collocation (http://www.netspace.co.nz) is around \$200. Full time support costs \$500 per month. Most of the traffic is national and is unlimited. Therefore the costs are \$10500 + \$900 per month.
    \paragraph{Benefits} 
        In our case, the only apparent reason for owning real servers is the full control over them and the slightly better performance of unvirtualized hardware.
                
    \paragraph{Risks}
        \subparagraph{Wrong load estimation and scalability issues}
        The main problem with purchasing the servers for startup is that it is hard to predict, which level of performance will be required in a few months. For example, you might find that several less powerful servers would better suit your needs than a single one. The opposite is quite possible --- you will have to buy a new much more powerful server and guess, what to do with the old ones. This risk is quite high and it will cost you not only a price of new servers, but also the burden of migration from one physical server to another.
                
        \subparagraph{Reliability and faults policy}
        In case of two servers (production and preproduction) there is some form of redundancy and it is possible to reassign roles in case of hardware failure. But this will essentially stop the usual development process until new server will be bought and setted up. This risk is relatively low, but the consequences are quite severe.
    
\subsubsection{Renting servers}  
    \paragraph{Costs}
        The cost of a reasonable server starts from \$400 per month. Most of the time it comes with the Linux operating system preinstalled. The cost of support varies from \$150 per month to \$500 per month. Once again, the national traffic is unlimited. In our case the cost can be estimated as \$1300 per month with negligible setup fees.

    \paragraph{Benefits}
        \begin{enumerate}
            \item Flexibility: there is no need to carefully choose and buy a quite expensive server outright.
            \item The cost of renting is comparable with the cost of collocation of your own server.
            \item More standard services from the hosting company might completely take away the need in proper system administrator.
        \end{enumerate}
    
    \paragraph{Risks}
        If there is a need in the server configuration which differs from a set of standard ones, it might cost a significant sum. In our case this risk is quite low.
        
        
\subsubsection{Renting virtual instances}
    \paragraph{Costs}
        A couple of Amazon's m3.xlarge instances (4 cores, 15 Gb of RAM, SSD 2 x 40) with additional 1 Tb of storage space and reasonable 20Gb of outgoing traffic will cost \$400 USD per month which is roughly \$530 NZD. Taking into consideration the ease of the server setup, the initial cost might be neglected.
        
    \paragraph{Benefits}
        \subparagraph{Fast deployment and maximum flexibility.}
        While the renting of real servers is a matter of days, renting virtual instances is a matter of hours. What is also important, getting rid of or upgrading resources is almost as easy as renting. This is essential, because being a startup, Regen should be able to quickly evaluate feasibility of new services and ideas and cancel them as soon as they prove to be unworthy.
        
        \subparagraph{Reliability.}
        Virtual instances are as reliable as a real ones, but in case of a failure, the time to get a new working server is limited only by the efficiency of backup system.
        
        \subparagraph{Additional services (Software and Platform as services).}
        Numerous services, that would otherwise require hours of system administrator's work, are already available for free of for a minimal fee. To name a few: automatic email sending, scheduled disk operations, complex monitoring and notifications of all sorts.

    \paragraph{Risks}
        \subparagraph{Limited configurations.} Being a small client, it is almost impossible to get an exotic server configuration from major cloud providers. Local and small providers are more flexible, but then again, the price will be much higher than with private server. Fortunately this risk is almost negligible in our case.
        
        \subparagraph{Network connectivity.} Many largest Cloud providers have their data centres oversees, with closest locations like Sydney or Hong Kong. Therefore traffic from sensors will travel from New Zealand to outside, and results will travel back. This might cause service delays in case of connectivity issues. This risk is a serious one, but can be mitigated by relatively easy migration to local suppliers, which cost almost the same, but with less additional services (http://www.sitehost.co.nz). 
                
        \subparagraph{Legal issues.} Sensor data is definitely a private information, but it is not under jurisdiction of laws about personal information. On current state this is not a real risk.
        
        \subparagraph{Security issues.} The main risk is not in the fact, that virtual instances on shared host are more vulnerable, then real machines, but in fact, that most system maintenance might be done by developers themselves without help of a professional system administrator. The obvious way to deal with this issue is to periodically order external security audit.

\subsection{Comparison and decision}

    \begin{table}[htb]
        \begin{tabular}{r|c|c|c}
            ~              & Costs     & Benefits  & Risks  \\ \hline
            Buy servers    & High      & Low       & High   \\
            Rent real      & Medium    & Medium    & Low    \\
            Local virtual  & Low       & Medium    & Low    \\
            Remote virtual & Low       & High      & Medium \\
        \end{tabular} 
        \caption{Comparison for Regen case.}
    \end{table}

    First of all, let's exclude the idea of buying own servers, because no benefits are really important and are strongly outweighed by risks and costs.
    
    For Regen renting real servers doesn't bring a lot of benefits, compared with virtual ones, but costs are considerably higher.
    
    Choosing between local and remote virtual instances we should decide what is more important: slightly better reliability and network latency or more services out of the box. At the current state more services means earlier start (crucial for startup), while response time and occasional misses are hardly noticeable with the poor quality of sensor network itself.
    
    That is why Regen is successfully using Amazon EC2 and Amazon Web Services till the current time (with secondary backups stored on Harmonics' private servers).

\clearpage
\section{Final conclusion}

After the brief analysis of two real life cases we can see, that Cloud solutions might be interesting not only to small companies without its own physical infrastructure (which correlates with the \cite{rashmi2012five}), but also to large corporations with data centres all over the world. 

Interesting, that both examined companies have chosen the Amazon Web Services as a provider. The main factor for using Cloud for both companies was resource flexibility and ease of provisioning. The second factor shared by both cases was cost. Both Regen and Kaspersky have found, that while the data and algorithms they use are confidential, they are ready to take the risk of compromised security, which was outweighed by gained benefits. 

For Kaspersky, independent nature of virtual instance suited the independent nature of analysts' project itself, and there were no issues with corporate network integration.

Regen was attracted not only by provided infrastructure (IaaS), but also by other tools of Amazon platform, that resolved many everyday administration routines (PaaS).



\bibliography{cloud_migration}
\end{document}








