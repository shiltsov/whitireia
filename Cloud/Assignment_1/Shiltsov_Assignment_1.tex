\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{apacite}
\usepackage{url}
\usepackage{enumitem}
\bibliographystyle{apacite}

\usepackage{/home/shield/Whitireia/whitireiatitle}

\title{Usability of cloud computing in high performance computation}
\author{Dmitry Shiltsov}
\course{IT8412 -- Cloud Computing and ICT Infrastructure}
\studentid{21402582}


\begin{document}

\begin{titlepage}

\whitireiatitle{Assignment 1}{}

\end{titlepage}

\begin{abstract}
    In this article we discuss several reasons for popularity of cloud solutions in high performance computation (HPC), enumerate benefits for independent researchers and name most apparent issues in security and performance. We come to conclusion that cloud computing is not applicable in many cases or requires additional efforts.
\end{abstract}


\section{Introduction}

\subsection{The reasons of popularity of cloud computing}
The cloud computing is one of the most exciting topics in IT industry for at least 5 years. It can change the significant part of the industry by providing computing as a utility, making concept of software as a service even more appealing and even influencing the way computer hardware is designed. Entrepreneurs don't have to make huge capital investments into hardware and IT staff anymore in order to start different sorts of Internet based businesses. Cloud computing effectively mitigates the risks of over-provisioning, when one overestimates the popularity of his service and has to pay for resources he doesn't need, and under-provisioning, which might lead to missed clients and lost of potential income. But what about researchers and other people who rely on resource demanding computation? They can rent hundreds of servers for a single hour and almost instantly perform the computation for the same price as a single server will cost them for hundreds of hours. While the idea of massive parallel calculation is not new, this is the first time a person (or a team) can get an access to resources of such scale not being part of organization or institution \cite{armbrust2009above}.


\subsection{Defining the scope}
Cloud computing is a loosely defined term. In this article we will use the definition according to \citeA{Mell_2011_The_NIST_definition_of_cloud_computing}:

\begin{quotation}
Cloud computing is a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction.
\end{quotation} 

The article of \citeA{Mell_2011_The_NIST_definition_of_cloud_computing} also sets up a number of essential characteristics: on-demand self-service, broad network access, resource pooling, rapid elasticity, measured service, service models: Software as a Service, Platform as a Service, and Infrastructure as a Service, and deployment models: private, public, community, hybrid. While many ``Platform as a Service'' and even ``Software as a Service'' solutions are applicable for research purposes, we will put the emphasis on ``Infrastructure as a Service'' as the most versatile approach.

\subsection{Benefits of Cloud computing}

Despite the progress in computer hardware, the power of a single machine is not enough in many areas. That is why many algorithms were redesigned to use the combined power of multiple computers \cite{Skillicorn_1999_Strategies_for_parallel_data_mining}. But to run software based on that algorithms, one requires a proper set of hardware. Up until early 2000's most academic institutions and enterprises owned special computer grids or clusters of commodity machines to run parallel applications \cite{computing2005distributed}. Usually the access to such resources was limited only to members or employees, the scheduling was tight, the time and budget to build or upgrade such grid was considerable. No doubt the Cloud Computing approach attracted lots of attention in researchers' society. 

The work of \citeA{vogels2008head} explains main differences of Cloud Computing compared with traditional grids:
\begin{itemize}[noitemsep]
    
    \item The illusion of infinite computing resources available on demand which means that there is almost no need to plan resource provisioning in advance.
    
    \item The lack of entrance threshold which allows to start small and increase computational performance as needed.

    \item The ability to pay for hardware on hourly basis which dramatically changes the strategies of use.
    
\end{itemize}

According to \citeA{venters2012critical} these differences lowered transactional costs and therefore supported creativity and innovations. Experiments that took months to prepare due to resource provisioning now can be run in a matter of hours. That is why many publications found cloud solutions useful for research tasks despite a numerous drawbacks  \cite{juve2009scientific}.

The major part of clouds' success lays in the adaptation of powerful software frameworks like Apache Hadoop and Apache Spark \cite{Dittrich_Efficient_Big_Data_Processing_in_Hadoop_MapReduce, Thusoo_Hive_a_petabyte_scale_data_warehouse_using_hadoop, Venner_Pro_Hadoop,Zaharia_2010_Spark_cluster_computing_with_working_sets,Zaharia_2012_Fast_and_interactive_analytics_over_Hadoop_data_with_Spark}, initially developed for cluster environment, to the new stack of technologies. 


\section{Challenges}

Along with the countless praises to cloud computing even more critics arose. We will stay away from discussion about the moral dilemmas about using the cloud and concentrate purely on practical obstacles which stand in front of any researcher shifting to cloud. The work of \citeA{armbrust2009above} enumerates some problems:

\begin{itemize}[noitemsep]
    \item Availability of Service;
    \item Data Lock-In;
    \item Data Confidentiality and Auditability;
    \item Data Transfer Bottlenecks;
    \item Performance Unpredictability;
    \item Scalable Storage;
    \item Bugs in Large Distributed Systems;
    \item Scaling Quickly;
    \item Reputation Fate Sharing;
    \item Software Licensing.
\end{itemize}

Lets take a closer look on some problems.

\subsection{Data lock-in}

Using standard technology stack allows more or less comfortable transfer between different cloud providers using manuals with different level of complexity \cite{catalyst,azure}. The next step is using another level of abstraction over the cloud, called ``metacloud'' in the work of \citeA{satzger2013wind}, and ``supercloud'' in the article of \citeA{jia2015supercloud}. This approach allows almost seamless transaction of data and virtual machine from one provider to another. This not only eliminates the risk of vendor lock, but also increases the efficiency of the system reducing latency and costs. 

\subsection{Legal issues}

In many cases research is conducted over the publicly available data, but sometimes the data might be very sensible. The publication of \citeA{mowbray2009fog} covers some legal issues associated with using cloud solutions. First of all, governments sometimes demand that data should be stored inside the countries and customers can't always control where the data is physically stored. Secondly, most user agreement are written in favor of service providers and individual researchers or small institutions can not change it. Data ownership is another issue that should be clearly defined in advance. Moving to cloud rises many more legal questions, but even enumerating all of them is out of the scope, so article seriously advices to consult lawyers all the time.

\subsection{Data confidentiality and auditability}

\subsubsection{Most notable security issues}

Most benefits of using cloud comes from sharing resources like hardware which undoubtedly involves security risks on each level of technology stack. 

One of the cornerstones of cloud computing is a virtualization. Unfortunately modern virtualization techniques are prone to vulnerabilities from the lowest level. The paper of \citeA{percival2005cache} demonstrated the access from one execution thread to another one through the cache of the modern processor, which can lead to the theft of cryptographic keys. This risk is further increased by the results obtained by \citeA{ristenpart2009hey} who managed to find the way to predict  the placement of virtual machine in the Amazon cloud. This allows an attacker to get close to specific victim. Setting your virtual machine on the same physical server with your target opens a number of attack possibilities not limited only by shared cache mentioned above.

Risks can appear in any aspect which involves sharing. For example, the Amazon EC2 provides multiple ready to deploy images of operating system and allows anyone to create and share such image. In the work of \citeA{balduzzi2012security}, researchers checked more than 5000 images and found a lot of issues concerning security and privacy for users and creators of images. The most apparent problem was in the outdated software 'frozen' into the images. This software has lots of well known vulnerabilities. Few images (based on Microsoft Windows) even contained trojan malware. Many systems established connections to other machines right out of the box. It is difficult to distinguish legitimate services from backdoors. From the other side, the privacy of images' creators can be compromised. The authors have found, that many virtual machines kept private keys, browser and shell history and even private documents. Another issue with usage of pre-configured images is that they provide enough of machine fingerprints, which help a possible attacker to identify its victim.


As \cite{mowbray2009fog} explains, cryptography is an obvious answer to most privacy concerned problems. But this solution is far from perfect for the number of reasons. First of all, process of encryption, transfer, storage and then unencryption is more complicated than just transferring and storing. And in the end, data still should be unencrypted at some stage of processing. Secondly, some sort of protection might be broken by the computational power governments possess. This is particularly possible in the case of virtual machines, where even cryptography is not as reliable (problems with bad randomness source) and requires additional measures \cite{ristenpart2010good}.



\subsubsection{Dealing with issues}

In their work \citeA{zissis2012addressing} claim to solve most security issues by using a trusted ``Third Party'' which manages Public Key Infrastructure for all security levels, Single-Sign-On authentication, which is perfect for distributed environment, and LDAP (lightweight directory access protocol).

\citeA{zhang2014secure} use the similar principle by improving the OpenStack Access Control model. To deal with secure information and resource sharing, they have added Secure Isolated Domain and Secure Isolated Projects entities which provide control over communication between different organizations. At the time of the publication the implemented system lacked ``fine-grained access control'', like access to virtual machines and networking.




\subsubsection{Security Audit}

Based on their research of Attack Graphs, \citeA{bleikertz2010security} implemented a tool which checks configuration of of Multi-tier Virtual Infrastructures in Public Infrastructure Clouds. Using the tool on the sample application in Amazon EC2 successfully detected a number of real vulnerabilities.

The work of \citeA{xiao2013structural} presented a privacy-preserving structural-reliability auditor (P-SRA) which demonstrated a good performance in case of small-scale cloud services.

DACSA (a Decoupled Architecture for Cloud Security Analysis) invented by \citeA{gionta2014dacsa} is ``Out-of-VM'' tool which can help with security analysis. It has a minimal performance overhead and might be used in production environment. The idea of the tool is to produce fast snapshots of VM, which can be used by any traditional off-line investigation.



\subsubsection{Real cases}

If we take few recent publications about doing research and computation in cloud environment, we might find that most of them include cryptography. For example \citeA{cao2014privacy} put a lot of efforts to implement multi-keyword ranked search over encrypted data in the cloud. Such sort of search is applicable even in cases where sensible information is involved. Another example is the study of \citeA{kuzu2015distributed}, who implemented parallel search over encrypted Big Data, including secure and efficiently updatable index and fine authorization system. The work of \citeA{liu2015privacy} presents the privacy-preserving technique to transform data in the way that it can be used in MapReduce service by untrusted cloud providers. DBMask by \citeA{sarfraz2015dbmask} is a solution which finely manages an access control in the SQL environment. The most prominent feature of the solution is that it can be used alongside with standard database engine. 


\subsection{Performance issues}

The main interest for researcher in the cloud technology lays in the ability to inexpensively perform distributed computation. Unfortunately the performance of virtual instances sometimes is significantly lower than characteristics advertised by cloud providers \cite{Ostermann_2010_A_performance_analysis_of_EC2_cloud_computing_services_for_scientific_computing,Iosup_2011_Performance_analysis_of_cloud_computing_services_for_many_tasks_scientific_computing}. 

The work of \citeA{garcia2015automated} deals with that issue by introducing an approach of selecting the right level of cloud instance according to the  level of hardware used before moving to cloud. This approach also enables price comparison for different cloud providers. 

Another way to get more computing power keeping the same budget is the algorithm suggested by \citeA{marathe2014exploiting}. This algorithm exploits the EC2 Spot Market and allows renting expensive instances up to seven times cheaper.

Unfortunately \citeA{mehrotra2012performance} demonstrates that in some cases the scalability of distributed system built in the cloud environment might be negative due to poor network connectivity. It means that adding more computational nodes increases the time of calculation. % here we might insert anappropriate picture

Another problem is in performance inconsistency. As \citeA{ou2012exploiting} discovered, a single type of instance in Amazon EC2, located on miscellaneous underlying hardware, might produce very different performance results with the variation reaching up to 60\%. 

 

\section{Conclusion}

As we stated in the introduction, the main issue a researcher wants to resolve with the use of cloud services is a quick and affordable access to high performance computation. Is cloud a good solution for such issue? Our answer is 'sometimes'. While the resources acquisition is really fast, the quality of this resources is very questionable. Looking at the articles like \citeA{Sheth_2012_Implementing_Parallel_Data_Mining_Algorithm_on_High_Performance_Data_Cloud}, we come to conclusion, that new approach can't fully replace traditional clusters. And to make things even worse, security considerations force significant changes to all distributed algorithms originally developed to be run in safe environment. And let's not forget all the legal questions. But for many projects based on open source software and publicly available data the privacy is not an issue, and with careful performance management cloud solution might be perfect for them. Another positive effect of enormous interest to the market of rented computing is that the pace of progress is really good. Lots of solutions are being proposed each year and cloud becomes more attractive to wider and wider audience. 

 

A very promising alternative to current approach in cloud resource organization came with the work of \citeA{omote2015improving}. Authors implemented the way to automatically install and configure any required operating system on a real hardware. It allows the fast and flexible way to manage resources with traditional performance and security of 'bare-metal' servers. 


\bibliography{cloud_security,cloud_usage,cloud_research,parallel_mining}

\end{document}