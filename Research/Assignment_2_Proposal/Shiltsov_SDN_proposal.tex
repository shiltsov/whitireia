\documentclass[a4paper,jou,natbib,apacite]{apa6}
\usepackage[utf8]{inputenc}

\title{Applying Common Open Research Emulator to SDN emulation using a cluster of blade servers}
\shorttitle{Applying CORE to SDN emulation}
% Think about the title

\author{Dmitry Shiltsov}
\affiliation{Whitireia New Zealand}

\abstract{}

\keywords{Software defined networking, SDN, Network emulation, Network Debugging, CORE}

\begin{document}

\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\center % Center everything on the page
 
%----------------------------------------------------------------------------------------
%   HEADING SECTIONS
%----------------------------------------------------------------------------------------

\textsc{\LARGE Whitireia New Zealand}\\[1.5cm] % Name of your university/college
\textsc{\Large IT8401 – Research in Information Technology}\\[0.5cm] % Major heading such as course name
\textsc{\large Research proposal}\\[0.5cm] % Minor heading such as course title

%----------------------------------------------------------------------------------------
%   TITLE SECTION
%----------------------------------------------------------------------------------------

\HRule \\[0.4cm]
{ \huge \bfseries Applying Common Open Research Emulator to SDN emulation using a cluster of blade servers}\\[0.4cm] % Title of your document
\HRule \\[1.5cm]
 
%----------------------------------------------------------------------------------------
%   AUTHOR SECTION
%----------------------------------------------------------------------------------------

\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Author:}\\
Dmitry \textsc{Shiltsov} % Your name

21402582

\end{flushleft}
\end{minipage}
~
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervisor:} \\
Dr. Marta \textsc{Vos} % Supervisor's Name
\end{flushright}
\end{minipage}\\[4cm]

% If you don't want a supervisor, uncomment the two lines below and remove the section above
%\Large \emph{Author:}\\
%John \textsc{Smith}\\[3cm] % Your name

%----------------------------------------------------------------------------------------
%   DATE SECTION
%----------------------------------------------------------------------------------------

{\large \today}\\[3cm] % Date, change the \today to a set date if you want to be precise

%----------------------------------------------------------------------------------------
%   LOGO SECTION
%----------------------------------------------------------------------------------------

\includegraphics[width=0.2\linewidth]{Whitiriea_Logo_Trans_BlackLetters_2662x3096-For_Other_Background.png}\\[1cm] % Include a department/university logo - this will require the graphicx package
 
%----------------------------------------------------------------------------------------

\vfill % Fill the rest of the page with whitespace

\end{titlepage}

\maketitle


\section{Introduction}

\subsection{Motivation}
%The motivation for the research (why this research is needed, why it is important)

\subsubsection{Software Defined Networking (SDN)}
This is a new approach in building and managing networks. The general underlying idea is to separate network's control and data forwarding planes to make it easier to optimize each. The network controller provides abstract centralized view of the whole network and determines the way in which switches or routers handle the traffic. According to \citeA{Feamster_The_Road_to_SDN} this approach should resolve a large number of problems in networking especially applied to large scale networks. Work of \citeA{Gupta_SDX_A_Software_Defined_Internet_Exchange} supports this statement by reporting about successfull implementation of SDN solution in a complex environment of Internet Exchange in Wellington with hundreds of peers connected. 

\subsubsection{Network emulation}
To reduce costs and risks, emulation is usually being used prior to deployment on real network. Despite being relatively new SDN is already well supported by different emulation approaches. For example on top of flow simulator (fs) for traditional networks \citeA{Gupta_Fast_Accurate_Simulation_for_SDN_Prototyping} developed a fs-sdn solution sacrificing some realism in order to achieve high performance in experimentation. The more popular solution described by \citeA{Lantz_A_Network_in_a_Laptop_Rapid_Prototyping_for_Software_defined_Networks} is Mininet -- a complete system available to download, run and build upon. For virtualization it uses Linux processes and virtual Ethernet pairs in network namespaces. The same method (OS container virtualization) is introduced by \citeA{Zec_Operating_system_support_for_integrated_network_emulation_in_IMUNES} in Integrated Multiprotocol Network Emulator/Simulator. It uses proved by many years ``jails'' mechanism of FreeBSD with kernel-level network stack virtualization technology. 

\subsubsection{Emulating large scale SDN}
Compared with traditional networks, SDN introduce additional problems in emulation. Scaling approach \cite{Pan:2005:SME:1103543.1103547}, which greatly helps to reduce the number of virtual network nodes is complicated by the nature of SDN: separation of planes. \citeA{Heller_The_Controller_Placement_Problem} state, that the whole behavior of the network can depend of the control plane, which should be modeled as accurately as data plane. Moving to the larger scale emulation \citeA{Roy_Challenges_in_the_Emulation_of_Large_Scale_Software_Defined_Networks} discovered even more challenges.  With limited resources of a host computer, increasing the number of virtual nodes leads to wrong results: errors that will inevitably strike the real system are hidden, but extra errors appear provided only by the emulation itself. The most obvious way to deal with the lack of resources (while keeping the idea of emulation) was suggested by \citeA{Puljiz_IMUNES_based_distributed_network_emulator}. This work relied on the idea of combining computational performance of several physical machines effectively distributing virtual network across them. The same underlying idea is used in Common Open Research Emulator (CORE) presented by \citeA{Ahrenholz_CORE_a_real_time_network_emulator}.


\subsection{Purpose}

Our intention is to design SDN emulation system on top of multiple machines connected with a real network and evaluate its performance considering all the challenges mentioned in previous section. This work may potentially help with testing and debugging of large scale research or enterprise software defined networks.



% Method
% Artefact Description (proposed solution/treatment/ design…)
% May include a tentative design/development (design validation and implementation).
% Evaluation
% Discussion
% Conclusion

\section{Construction of SDN emulation system}

\subsection{Method}
We are going to use Common Open Research Emulator and start adding machines from Whitireia cluster one by one. In the process we will measure the maximum emulating capacity (in the number of network nodes) until effects described in \citeA{Roy_Challenges_in_the_Emulation_of_Large_Scale_Software_Defined_Networks} start to appear. Using those measurements we will be able to evaluate the dependency of the performance and the number of machines in the system. 

\subsection{Design details}

\subsubsection{Whitireia cluster}
The cluster consists of 320 blade servers. Each server is a 3.4GHz Xeon dual CPU with hyper threading and 8GB of RAM. Servers are connected using 
Nortel and IBM Switch Management Modules with Juniper EX series switches. Each processor, being only single core, allows us to detect performance boost very accurately. In the mean time all the blades provide us with a really broad experimentation range.

\subsubsection{CORE}

We will use Common Open Research Emulator because of its particular features \cite{Ahrenholz_CORE_a_real_time_network_emulator}:
\begin{APAitemize}
    \item Open Source.
    \item Distributed with multiple COREs.
    \item Runs applications and protocols without modifying them.
    \item Centralized configuration and control.
\end{APAitemize}

\subsubsection{Network topology}

We will start with the simple network scheme used by \citeA{Gupta_Fast_Accurate_Simulation_for_SDN_Prototyping} in their experimentation. This scheme is illustrated with Figure~\ref{fig:top1}. Virtual switches are just connected to a single line. In the process of evaluation of the maximum capacity we will increase the number of virtual switches simultaneously in each blade server.

\begin{figure}[h]
\includegraphics[width=0.9\linewidth]{topology1.png}
    \caption{\label{fig:top1} Linear network topology }
\end{figure}

Another run will include more complex network structure (Figure~\ref{fig:top2}). The distinctive feature of this structure is keeping number of connection between blade servers as low as possible (ideally single link).

\begin{figure}[h]
\includegraphics[width=0.9\linewidth]{topology2.png}
    \caption{\label{fig:top2} Isolated network topology }
\end{figure}

The final run will include network topology with lots of connections between blade servers (Figure~\ref{fig:top3}). This case will put the heavy load on the physical network, connecting blades together.

\begin{figure}[h]
\includegraphics[width=0.9\linewidth]{topology3.png}
    \caption{\label{fig:top3} Meshed network topology }
\end{figure}

\subsubsection{Analysis of data}
All the data acquired from the experiments (topology, number of servers, maximum number of nodes) will be stored as a table in CSV-file. This data will be processed using R statistical environment \cite{team2012r} in order to build and test scalability models and plot outcome results.



\subsection{Scope statement and delimitations}

\subsubsection{The emulation approach}

CORE system provides us with a very high level of realism, but it comes by the cost of raw performance. Modern network simulation software can support the larger number of nodes given the same resources. It means, that using the same hardware, one might achieve better performance results while keeping the accuracy of the experiment on the same level.

\subsubsection{The underlying hardware}

While having in theory 320 servers in the cluster, for this experiment we can  really use only part of them (less than 50) because we have to share the cluster with other users. Another point is that while being very convenient for scaling evaluation Whitireia blade servers would definitely provide results different from up-to-date multi-core servers because the latter will lack the limitations caused by the large number of physical connections. This is strongly related with the following paragraph.

\subsubsection{The network topology}

As discussed by \citeA{Puljiz_IMUNES_based_distributed_network_emulator} the mapping of the scheme of the modeled network and the physical network connecting the cluster could seriously affect the whole performance. With the linear model of the network taken from the work of \citeA{Gupta_Fast_Accurate_Simulation_for_SDN_Prototyping} we might quickly exhaust the bandwidth of real network connections between servers in the cluster. The difference in connection speed between real and virtual part of the network might cause additional difficulties in scaling the system.



\subsection{Ethical considerations}
The main idea of research is to build and test the artifact with no human beings involved in the process.


\subsection{Intellectual property considerations}
R language core is licensed under Gnu Public License (GPL). Licensing of other modules may vary, but most of them are covered by GPL, Massachusetts Institute of Technology license (MIT) or Berkeley Software Distribution license (BSD).

The CORE system has been released by Boeing to the open source community under the BSD license. This means that we are not only able to use it for research purposes but even can build commercial solution with a closed source. 




\bibliography{sdn_in_real_life}



\end{document}
