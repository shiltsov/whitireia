\documentclass[a4paper,natbib, man, apacite]{apa6}
\usepackage[utf8]{inputenc}

\title{Literature review on Software Defined Networks implementation}
\shorttitle{SDN implementation}


\author{Dmitry Shiltsov}
\affiliation{Whitireia New Zealand}
\date{\currenttime}

\abstract{In this review we will familiarize ourselves with Software Defined Networks (SDN) and their applicability in real life. We will discuss challenges in SDN implementation and list useful tools to overcome these challenges. Cases of successful deployments will be presented.}

\keywords{Software defined networking, SDN, Deployment, OpenFlow, Network Virtualization, Network Debugging, Mininet}

\begin{document}

\maketitle



\section{Introduction} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{What is SDN}

Software Defined Networking (SDN) is a new approach to building and managing networks. The general underlying idea is to separate network's control and data forwarding planes to make it easier to optimize each. The network controller provides abstract centralized view of the whole network and determines the way in which switches or routers handle the traffic. Implementation of this principle with OpenFlow protocol is illustrated by Figure~\ref{fig:openflow} \cite{McKeown_OpenFlow_Enabling_Innovation_in_Campus_Networks}.

\begin{figure}[h]
    \fitfigure[]{openflow.pdf}
    \caption{\label{fig:openflow} Idealized OpenFlow Switch. The Flow Table is controlled by a remote controller via the Secure Channel.} 
\end{figure}

\subsection{The goal of literature review}
In the Internet SDN is being praised as a universal solution by commercial companies providing related hardware and software. But through reading popular articles, technical forums and comments of real system administrators one can be surprised by the level of criticism on the new approach. The most popular reason for rejection is unreadiness of the technology to real life due to poor performance, security, reliability and lack of diagnostic tools. This review is intended to determine the state of Software Defined Networking from pragmatical point of view. Using academic sources we will look for history of successful implementation of SDN, check the possibilities to manage, install or migrate to the new technology. This could also help in evaluating the chances of the technology to succeed.






\section{History of SDN implementation} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection {The Road to SDN}
The great article of \citeA{Feamster_The_Road_to_SDN} highlights the reasons for accepting or abandoning networking technologies. It explains origins of SDN with help of historical overview of closely related inventions.

\subsubsection{Active Networking}
It was the first approach to programmable networking. This was quite a radical way to control exposed resources like processing, storage and packet queues on separate network nodes through programming interface. Being a big move from traditional IP stack it was heavily criticized for inadequate complexity by Internet community. This complexity was partially provided and compensated by improvements in cross-platform programming languages like Java which brought portability and relative code safety in the virtual machine environment. In the end Active Networking was abandoned as impractical mainly due to lack of clear usage scenarios and absence of incremental implementation strategies. Nevertheless Active Networking contributed to the network technology the following ideas:
\begin{APAitemize}
    \item Programmable functions in the network for greater flexibility.
    \item Network virtualization and splitting of data-flow based on package headers.
    \item The vision of an orchestrated architecture, controlled from inside.
\end{APAitemize}


\subsubsection{Separation of data and control planes}
By contrast with previous approach this one was purely pragmatical. The main goal of separating data and control planes on each device was to improve traffic engineering (the traffic delivery paths). It was aimed primarily to help system administrators in debugging, predicting and controlling routing behavior. Introduction of open interface between control and data planes and logically centralized control of the network answered the number of actual issues: 
\begin{seriate}
    \item support of new equipment with packet-forwarding logic implemented in hardware,
    \item increasing size of Internet service providers networks,
    \item demand for greater reliability,
    \item new services like virtual private networking.
\end{seriate}


\subsubsection{OpenFlow}

According to the article this instantiation of SDN principles had found right balance between vision of fully programmable networks and real life applicability. The core idea of technology is Open Flow switch -- device containing a table of packet-handling rules, based on the packet headers. One of the most attractable features of OpenFlow implementation was using existing hardware. Most of ordinary switches could be turned into the OpenFlow switch by firmware update. It proved to be more cost-effective to run a large number of commodity switches then purchase closed proprietary equipment. Started in academical circles the technology was scaled to manage network of a typical campus. Main achievements of OpenFlow are:
\begin{APAitemize}
    \item Idea of universal network device for broad functionality like firewall or router.
    \item The vision of a network operating system abstracting logic and applications from state in network switches.
    \item Distributed controllers, acting as a single logical one.
\end{APAitemize}



\subsubsection{SDN and Network virtualization}

Network virtualization is the abstraction of network from the underlying physical equipment. While virtualized networking exists apart from SDN in the form of overlay networks, and SDN generally doesn't imply virtual networks, using both of these approaches in one stack generates huge benefits. There are three general ways to utilize these benefits:

\begin{APAitemize}
    \item Virtual Networks are easily implemented by SDN.
    \item Network virtualization for evaluating and testing SDN before moving to production.
    \item Sliced networks. Sharing the same physical environment by independent parties.
\end{APAitemize}




\subsubsection{Future of SDN}

The article enumerates a lot of possible applications of SDN, but concludes, that the future of technology depends only on its popularity. While authors mention successful installations on home and enterprise networks, as well as cellular core networks and Internet Exchange points, they are aware that SDN is by no means a complete tool and requires a lot of innovations on different levels.


\subsection {Maturing of OpenFlow and SDN}

The sound work of \citeA{Kobayashi_Maturing_of_OpenFlow_and_Software_defined_Networking_through_deployments} is another story of the path the SDN went from paper to hardware. Starting from the proof of concept the article explains details of realization, connection of the first SDN with the rest of the world, issues of performance and design. Particularly there is detailed description of the sliced network implementation (mentioned by \citeA{Feamster_The_Road_to_SDN} in previous section). Work continues with successful demonstration of SDN scaling to the national level with connecting campuses throughout the territory of USA. Deployment in production network showed problems of stability or predictability. The hybrid approaches were tested via the next experiment in providing wireless networking environment. All this activity resulted in significant improvements of OpenFlow protocols. The article provides detailed information on performance issues and tools used for debugging purposes. 

The conclusion of the article is that despite all the successes, it is still in an early stage of development and the full potential is to be explored in the next few years.



\section{Virtualization and testing} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The important part of implementing SDN is the ability to design and test it in virtual environment and the transfer it to real hardware. Due to open nature of OpenFlow protocol and simple underlying principles there is plenty of solutions covered by numerous articles.


\citeA{Lantz_A_Network_in_a_Laptop_Rapid_Prototyping_for_Software_defined_Networks} presented Mininet -- a complete system available to download, run and build upon. For virtualization it uses Linux processes and virtual Ethernet pairs in network namespaces. Authors have found this system quite efficient to launch networks with gigabits of bandwidth and hundreds of nodes (switches, hosts, and controllers).

\citeA{Canini_A_NICE_Way_to_Test_Openflow_Applications} developed a NICE -- testing suit for Python applications on the popular NOX platform. This application could uncover bugs in OpenFlow programs, through a combination of model checking and symbolic execution. 


\citeA{Teixeira_Datacenter_in_a_Box_Test_Your_SDN_Cloud_Datacenter_Controller_at_Home} have built a complex Data Center emulation environment around Mininet and POX platform. This system was aimed to test SDN Cloud Data Center controllers and was validated with well-known  virtual machine scheduling algorithms.


\citeA{Gupta_Fast_Accurate_Simulation_for_SDN_Prototyping} developed a simulation-based tool called fs-sdn and compared it with Mininet system. They came to conclusion that with the same level of accuracy as Mininet, fs-sdn shows significantly better performance.


\citeA{Drutskoy_Scalable_Network_Virtualization_in_Software_Defined_Networks} described virtualization based on using database to store Virtual Network mapping. This approach, called FlowN, proved to be extremely efficient starting with the large number of virtual networks (e.g., around 100).


\citeA{Salsano_OSHI_Open_Source_Hybrid_IP_SDN_networking_and_its_emulation_on_Mininet_and_on_distributed_SDN_testbeds} designed and implemented  Open Source Hybrid IP/SDN (OSHI) node useful in mixed case of SDN and traditional network. Authors demonstrated good result in Mininet and using distributed SDN research testbeds.


\citeA{Handigol_Where_is_the_Debugger_for_My_Software_defined_Network} introduced an ndb, a network debugger with two useful primitives taken from gdb (GNU Debugger): breakpoints and packet back-traces. Most prominent feature of ndb is that it is not aimed for any single layer or protocol and doesn't expect any particular language, primitive or framework.


\citeA{Kotronis_On_Bringing_Private_Traffic_into_Public_SDN_Testbeds} brought up an important question in the testing of SDN: experimenting with real user Internet traffic. Authors suggested Privacy and Availability Layer (PAL) as a way to deal with most obvious issues, but struggled with security problems.


\section{Real life cases} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The notable example of SDN implementation is described in \citeA{Stringer_Cardigan_Deploying_a_Distributed_Routing_Fabric}. They  deployed SDN based router and put it at a public Internet exchange in New Zealand, effectively connecting Research and Education Advanced Network of New Zealand (REANNZ) to the Wellington Internet Exchange. They observed delays and corruption in the process of installing flows due to specific exchange environment, but managed to find solution, which mitigates this problem.

Another case of Internet Exchange was implemented in \citeA{Gupta_SDX_A_Software_Defined_Internet_Exchange}. They started with accurate modeling via Mininet and then transfered resulting system to real Internet exchange point serving Amazon Web Services hosting used by the University of Wisconsin and Clemson University.

\citeA{Szyrkowiec_First_field_demonstration_of_cloud_datacenter_workflow_automation_OpenStack_and_OpenFlow_orchestration} demonstrated dynamic data center built on top of combination of OpenFlow and OpenStack (the leading open source cloud platform). Authors claimed that this solution reduced provisioning times in the data center from typical days or even months to mere tens of seconds.

\section{Conclusion} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this paper we familiarized ourselves with SDN, deeply examined the history of its implementation, highlighted some practical approaches (namely virtualization and debugging). Unfortunately, despite of all recent improvements in the technology we couldn't find a lot of real business cases. But taking into consideration the pace of development we expect more successful cases of SDN implementations on enterprise level in the next few years uncovering the full potential of the approach. We expect that increasing popularity of OpenStack solution may lead to popularity of SDN as supporting technology.


\bibliography{sdn_in_real_life}



\end{document}
