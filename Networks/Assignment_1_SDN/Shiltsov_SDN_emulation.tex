\documentclass[a4paper,jou,natbib,apacite]{apa6}
\usepackage[utf8]{inputenc}

\title{Software defined networks in virtual environment}
\shorttitle{SDN in Virtual Environment}
\author{Dmitry Shiltsov}
\affiliation{Whititreia New Zealand}

\abstract{Software Defined Networking (SDN) is a new approach in the network building and control. This article introduces reasons behind SDN and overviews some tools invented to configure and debug SDN. Significant part of the work is dedicated to network prototyping, where different techniques are described and compared.}

\keywords{Software defined networking, SDN, Deployment, OpenFlow, Network Virtualization, Network Debugging, Mininet}

\begin{document}

\maketitle



\section{Introduction} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Software Defined Networking (SDN) is a modern trend in academical and technical societies devoted to building and maintaining networks. This article introduces reasons behind the programming approach in network management. Then the evolution from the first attempts to the modern implementation is briefly described with the OpenFlow protocol as a de facto synonym for SDN being presented. After that article enumerates key characteristics for a good system prototype. Different emulation techniques applicable to Software Defined Networks are observed and compared according to mentioned characteristics.


\section{Software Defined Networking } %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The key feature of Software Defined Networking is the separation of control plane from the data plane. These terms and the whole concept will be explained in this section.


\subsection{Complexity in Networking}

In the beginning of the Internet network relied on relatively simple principles like best-effort delivery of IP. But with time new requirements emerged. Complexity brought by access control lists, VLANs, traffic engineering, firewalls, NAT was met by system administrators across the world. But despite this complexity, the approach in network management hasn't changed. In contrast, if we take software development through the last decades, we can observe apparent evolution in interaction of users and tools. Machine languages with no abstractions forced dealing with each tiny bit of lower-level details. With rising power of computers these languages were mostly superseded by high level languages with such abstractions as virtual memory, artificial data types and so on. With object oriented approach and garbage collectors modern languages are even further away from underling hardware. This progress resulted in making simple tasks even simpler and allowed creation of systems otherwise hardly possible. While interfaces don't eliminate complexity, they effectively hide it, allowing implementation freedom on both sides of interface. Returning to networking one can find a nice level of abstraction in data forwarding plane with all the OSI layers, but there are almost no reliable building blocks in control plane, which forces system administrator to solve each task individually.
        

\subsection{Road to SDN}

The article of \citeA{Feamster_The_Road_to_SDN} highlights steps to SDN made by community to solve the problem of exposed complexity mentioned in previous part. This work defines three main steps: Active Networking, Separation of data and control planes, OpenFlow. 

\subsubsection{Active Networking}
It was the first approach to programmable networking. This was quite a radical way to control exposed resources like processing, storage and packet queues on separate network nodes through programming interface. Being a big move from traditional IP stack it was heavily criticized for inadequate complexity by Internet community. This complexity was partially provided and compensated by improvements in cross-platform programming languages like Java which brought portability and relative code safety in the virtual machine environment. In the end Active Networking was abandoned as impractical mainly due to lack of clear usage scenarios and absence of incremental implementation strategies. Nevertheless Active Networking contributed to the network technology the following ideas:
\begin{APAitemize}
    \item Programmable functions in the network for greater flexibility.
    \item Network virtualization and splitting of data-flow based on package headers.
    \item The vision of an orchestrated architecture, controlled from inside.
\end{APAitemize}


\subsubsection{Separation of data and control planes}
By contrast with previous approach this one was purely pragmatical. The main goal of separating data and control planes on each device was to improve traffic engineering (the traffic delivery paths). It was aimed primarily to help system administrators in debugging, predicting and controlling routing behavior. Introduction of open interface between control and data planes and logically centralized control of the network answered the number of actual issues: 
\begin{seriate}
    \item support of new equipment with packet-forwarding logic implemented in hardware,
    \item increasing size of Internet service providers networks,
    \item demand for greater reliability,
    \item new services like virtual private networking.
\end{seriate}


\subsubsection{OpenFlow}

According to the article this instantiation of SDN principles had found right balance between vision of fully programmable networks and real life applicability. The core idea of technology is Open Flow switch -- device containing a table of packet-handling rules, based on the packet headers. One of the most attractable features of OpenFlow implementation was using existing hardware. Most of ordinary switches could be turned into the OpenFlow switch by firmware update. It proved to be more cost-effective to run a large number of commodity switches then purchase closed proprietary equipment. Started in academical circles the technology was scaled to manage network of a typical campus. Main achievements of OpenFlow are:
\begin{APAitemize}
    \item Idea of universal network device for broad functionality like firewall or router.
    \item The vision of a network operating system abstracting logic and applications from state in network switches.
    \item Distributed controllers, acting as a single logical one.
\end{APAitemize}

Next section highlights some details of the OpenFlow.


\subsection{OpenFlow principles}

As shown in Figure~\ref{fig:openflow} by \cite{McKeown_OpenFlow_Enabling_Innovation_in_Campus_Networks}, OpenFlow protocol is used by control plane to define data plane and to get information on current data plane state. Applications are set on top of control plane, managed by external set of policies.

\begin{figure}[h]
    \fitfigure[]{openflow.pdf}
    \caption{\label{fig:openflow} Idealized OpenFlow Switch. The Flow Table is controlled by a remote controller via the Secure Channel.} 
\end{figure}


\subsubsection{Flow processing logic}

Another key idea of OpenFlow displayed by \cite{McKeown_OpenFlow_Enabling_Innovation_in_Campus_Networks} is the Flow Table which is essentially a core of each switch. This table consists of rules, based on header fields of the packet and specifies the way the packet should be forwarded. If no rule is applicable to a packet, switch sends special message to controller. Controller then passes this message to particular application which resolves the problem either by sending out initial message or by modifying flow table (using another type of message). This process might be reactive, when FlowTable is modified only after the first ``unknown'' packet was received or proactive, when new rules can be set in advance. Together with topology-discovery algorithms this logic provides ``unprecedented visibility and control over the network dynamics'' \cite{McKeown_OpenFlow_Enabling_Innovation_in_Campus_Networks}.


\subsection{SDN popularity}
\citeA{Feamster_The_Road_to_SDN} enumerates a lot of possible applications of SDN, but concludes, that the future of technology depends only on its popularity which correlates with the existence of practical tools. This is covered by next section.



\section{Network prototyping and debugging} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

With introduction of networked applications and new protocols problem of their evaluation arises. To avoid occupation of production infrastructure or to avoid buying unnecessary expensive hardware, researchers rely on emulation \cite{Roy_Challenges_in_the_Emulation_of_Large_Scale_Software_Defined_Networks}. According to \citeA{Feamster_The_Road_to_SDN}, network virtualization is the abstraction of network from the underlying physical equipment. While virtualized networking exists apart from SDN in the form of overlay networks, and SDN generally doesn't imply virtual networks, using both of these approaches in one stack generates huge benefits:
\begin{APAitemize}
    \item Virtual Networks are easily implemented by SDN.
    \item Network virtualization for evaluating and testing SDN before moving to production.
\end{APAitemize}



\subsection{Prototyping}
    
\citeA{Lantz_A_Network_in_a_Laptop_Rapid_Prototyping_for_Software_defined_Networks} describe a good prototype. It should be flexible, with possibility to add new functions by software, preferably using familiar environment (OS, languages). Prototype should be deployable allowing migration to real hardware as is, keeping code and configuration the same. It is very convenient to have ability to interact with prototype in real time as with real network. Scalability of prototyping environment is an essential feature. Authors expect hundreds of switches being emulated by single computer. Another key feature is realistic behavior. This means ability to use applications and protocols without tweaking. And the last but not the least requirement is potential to share the work with other researchers, who can start their own experiments.

In the article of \citeA{Lantz_A_Network_in_a_Laptop_Rapid_Prototyping_for_Software_defined_Networks} there are four approaches to prototyping network are distinguished:

\begin{itemize}
    \item Special-purpose testbeds (GENI, VINI, PlanetLab, Emulab).
    \item Network of virtual machines.
    \item OS-level virtualization (Mininet, IMUNES).
    \item Simulators (ns-2, ns-3, fs).
\end{itemize}
    
    
    
\subsubsection{Special-purpose testbeds}


As explained by \citeA{Hibler_Large_scale_Virtualization_in_the_Emulab_Network_Testbed} in testbeds like Emulab and PlanetLab  virtual network is set on top of real hardware. The most apparent consequences are: 
\begin{itemize}
 \item usage of real operating systems and other software;
 \item running unmodified applications and real protocols;
 \item getting actual performance results.
\end{itemize}

The main limitation of this approach is scalability -- problems arise as soon as emulated network exceeds actual hardware. Another limitation is a lack of free access to such testbeds.



The performance of PlanetLab was further increased by \citeA{Turner_Supercharging_PlanetLab_a_high_performance_multi_application_overlay_network_platform} who added fast Network Processor (NP) subsystems to conventional servers of PlanetLab. After careful mapping of virtual and real nodes, one can achieve real performance of NP with programmer-friendly environment provided by original machines of the testbed.




\subsubsection{Network of virtual machines}

Using of conventional full-scale virtual machines is described in \citeA{Yap_OpenFlowVMS}. This allows almost the same level of flexibility and reality as a real network of machines, but \citeA{Lantz_A_Network_in_a_Laptop_Rapid_Prototyping_for_Software_defined_Networks} clearly states that scalability of this approach is unsatisfactory because of large memory overhead, allowing only few dozens of switches and hosts running on the best PC.

\subsubsection{OS-level virtualization}

This level of virtualization involves using internal OS mechanisms to partition resources of machine. On the one hand this means almost no resources overhead, on the other -- the choice of operational environment is restricted to host OS.

\paragraph{Mininet}  
\citeA{Lantz_A_Network_in_a_Laptop_Rapid_Prototyping_for_Software_defined_Networks} presented Mininet -- a complete system available to download, run and build upon. For virtualization it uses separate Linux processes and virtual Ethernet pairs in network namespaces. The main way to interact with the system is special command line interface (CLI). External tools that able to generate Mininet's scripts (Figure~\ref{fig:miniedit}) effectively add graphical user interface (GUI). Another way is using provided Python API. Example of Python application is shown in the Figure~\ref{fig:pyapp}. Authors have found this system quite efficient to launch networks with gigabits of bandwidth and hundreds of nodes (switches, hosts and controllers). Main limitation of the system (apart from using the same Linux kernel as a host system) is lack of performance fidelity: default Linux scheduler doesn't guarantee constant and uniform performance of each process. Another problem is that relatively high software performance of forwarding with O(n) can not match hardware-accelerated one with O(1).

\citeA{Teixeira_Datacenter_in_a_Box_Test_Your_SDN_Cloud_Datacenter_Controller_at_Home} have built a complex Data Center emulation environment on top of Mininet and POX (platform for the rapid development and prototyping of network control software using Python). This system was aimed to test SDN Cloud Data Center controllers and was validated with well-known virtual machine scheduling algorithms.

Mininet was chosen by \citeA{Salsano_OSHI_Open_Source_Hybrid_IP_SDN_networking_and_its_emulation_on_Mininet_and_on_distributed_SDN_testbeds} to test introduced Open Source Hybrid IP/SDN (OSHI) nodes useful in mixed case of SDN and traditional network.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{consoles_py.jpg}
    \caption{\label{fig:pyapp} Mininet Python application}
\end{figure}


        
\begin{figure}[h]
    \includegraphics[width=\linewidth]{miniedit2108.png}
    \caption{\label{fig:miniedit} MiniEdit application}
\end{figure}




\paragraph{IMUNES}  
\citeA{Zec_Operating_system_support_for_integrated_network_emulation_in_IMUNES} introduced Integrated Multiprotocol Network Emulator/Simulator using FreeBSD Jails with kernel-level network stack virtualization technology. Like Mininet system is an open source and also distributed as a complete VM image. Unlike Mininet IMUNES has a set of GUI tools (Figure~\ref{fig:imunes}) by default while CLI is still available. Authors showed indisputable advantage over the network of virtual machines.

        
\begin{figure}[h]
    \includegraphics[width=0.5\textwidth]{imunes.png}
    \caption{\label{fig:imunes} IMUNES visual tools}
\end{figure}



\subsubsection{Simulators}

In this approach one doesn't try to emulate particular parts of the network, but simulates the network as a whole.

The most famous network simulators ns2 and ns3 are covered by the work of \citeA{Henderson_Improving_Simulation_Credibility_Through_Open_Source_Simulations}. According to this work, ns2 was not very scalable or accurate but was still very popular in 2008. ns3 is aimed to solve all the problems of ns2, add modern API, add use of real code and even support distributed mode by using test beds.

\citeA{Canini_A_NICE_Way_to_Test_Openflow_Applications} developed a NICE -- testing suit for Python applications on the popular NOX platform. This application could uncover bugs in OpenFlow programs, through a combination of model checking and symbolic execution. 

On top of fs (flow simulator) \citeA{Gupta_Fast_Accurate_Simulation_for_SDN_Prototyping} developed a tool called fs-sdn. They compared it with Mininet system and came to conclusion that with the same level of accuracy as Mininet, fs-sdn shows significantly better performance.

\citeA{Drutskoy_Scalable_Network_Virtualization_in_Software_Defined_Networks} described virtualization based on using database to store Virtual Network mapping. This approach, called FlowN, proved to be extremely efficient starting with the large number of virtual networks (e.g., around 100).





 
\subsubsection{Hybrid approaches}  

\citeA{Puljiz_IMUNES_based_distributed_network_emulator} took IMUNES and made it distributed significantly increasing scalability. They also added ability for multiple users to run the cluster externally with cross platform GUI.

CORE (Common Open Research Emulator) by \citeA{Ahrenholz_CORE_a_real_time_network_emulator} added a whole list of improvements to original IMUNES:
\begin{itemize}
 \item wireless networks;
 \item mobility scripting;
 \item distributed emulation;
 \item remote API;
 \item control of external Linux routers;
\end{itemize}

In the end authors got hybrid solution which enabled experiments using ``wireless'' and ``wired'' connections in the same time.



By contrast with IMUNES based solutions in this section \citeA{Turner_Supercharging_PlanetLab_a_high_performance_multi_application_overlay_network_platform} introduces top-down approach adding OS-level virtualization to each machine of testbed. This solves the main problem of testbeds -- scalability. Also it allows more simultaneous experiments in the same time effectively reducing costs.
   
\subsection{Comparison}

Let's enumerate all desirable prototyping features and map them to emulation solutions.

\paragraph{Flexible} Simulators are natural leaders here. OS-level emulation can also present high flexibility. 
\paragraph{Deployable} Most modern approaches are quite good in this aspect. Some older simulators might cause troubles.
\paragraph{Interactive} As a basic requirement interactivity is supported by almost any approach.
\paragraph{Scalable} The apparent champions are simulators and the most limited option is testbeds.
\paragraph{Realistic} This is the area where testbeds shine. The least realistic choice is simulators.
\paragraph{Sharable} Testbeds are least accessible option here. Network of virtual machines might require significant resources and time to set up. All other approaches are very sharable by design.

This can be summarized by the Table~\ref{tab:comparison}.
\begin{table}[h]
    \begin{center}
        \begin{tabular}{|c|c|c|c|c|}
            \hline
            ~ & testbeds & simulators & VM & OS-level\\
            \hline
                Flexible & - & + & - & + \\
                Deployable & + & +/- & + & + \\
                Interactive & + & +/- & + & + \\
                Scalable & - & + & - & + \\
                Realistic & + & - & + & +/- \\
                Sharable & - & + & +/- & + \\
            \hline
        \end{tabular}
        \caption{\label{tab:comparison} Prototyping methods comparison}
    \end{center}
\end{table}


\section{Conclusion} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Full spectrum of prototyping approaches allows any researcher to choose the most appropriate solution. Some prototyping environments are especially user friendly and will definitely help SDN to get its popularity. Unfortunately according to \citeA{Roy_Challenges_in_the_Emulation_of_Large_Scale_Software_Defined_Networks} most solutions are still hardly useful in case of evaluation of large networks found in many data centers or enterprise networks. Authors demonstrated that shrinking an emulated Software Defined Network beyond certain limits leads to unrealistic performance of some nodes which may cause either false error detections or entirely missed errors.






\bibliography{sdn_in_real_life}



\end{document}
