\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{default}
\usetheme{Warsaw}
\usecolortheme{dove}


\title[SDN in virtual environment]{Software defined networks in virtual environment}
\date{\today}
\author{Dmitry Shiltsov}
\institute[Whitireia]{Whitireia New Zealand}


\begin{document}

\begin{frame}
    \titlepage
\end{frame}


\section{What is SDN}

\subsection{Dealing with complexity}

\begin{frame}
    \frametitle{Network complexity}

    \begin{itemize}
    \item Networks used to be simple:
        \begin{description}
            \item[Basic Ethernet/IP] straightforward, easy to manage.
        \end{description}
        
    \bigskip 
    
    \item New requirements have led to complexity:
        \begin{description}
            \item [ACLs] access control lists;
            \item [VLANs] virtual LANs;
            \item [TE] traffic engineering;
            \item [Middleboxes] firewalls, intrusion detection, NAT,\ldots;
            \item [DPI] deep packet inspection.
        \end{description}
    
    \end{itemize}
\end{frame}



\begin{frame}
    \frametitle{Programming evolution}
    
    \begin{itemize}
        \item Machine languages: no abstractions. % dealing with low-level details
        \bigskip   
        \item High-level languages: useful abstractions. % file systems, virtual memory, abstract data types, \ldots
        \bigskip 
        \item Modern languages: even more abstractions.  % object orientation, garbage collection, \ldots
    \end{itemize}

\end{frame}



\begin{frame}
    \frametitle{Abstractions and interfaces}
    
    Interfaces isolate programs from low-level details:
    \begin{itemize}
        \item freedom of implementation on both sides;
        \item modular program structure.
    \end{itemize}
    
    They hide complexity:     
    \begin{enumerate}
        \item someone deals with the problem once,
        \item everyone else uses that work.
    \end{enumerate}
  
\end{frame}


\begin{frame}
    \frametitle{Abstractions in networking}
    
    Data plane: OSI layers abstractions:
    \begin{itemize}
        \item IP -- best effort delivery;
        \item TCP -- reliable byte-stream.
    \end{itemize}
    
    \bigskip
    
    Control Plane: ?
    \begin{itemize}
     \item No sophisticated management building blocks.
     \item New control requirements cause increased complexity.
    \end{itemize}

\end{frame}

\subsection{SDN solution}

\begin{frame}
    \frametitle{General principles of SDN}
    
    \begin{itemize}
     \item Isolating data-plane from control plane.
     \item Making data-plane nodes as simple as possible.
     \item Centralizing control plane.
    \end{itemize}
    
    \centering
    \includegraphics[width=1\linewidth]{Traditional_to_SDN.png}
    
 
\end{frame}

\begin{frame}
    \frametitle{OpenFlow realization of SDN}

    \centering
    \includegraphics{openflow.pdf}

%      ``Maturing of OpenFlow and SDN'' by Kobayashi et al. (2014)
    
\end{frame}

\section{Network prototyping and debugging}

\subsection{Overview}

\begin{frame}
    \frametitle{Prototype}
    
    \begin{itemize}
        \item Flexible; % new topologies and new functionality should be defined in software, using familiar languages and operating systems.
 
        \item Deployable; % deploying a functionally correct prototype on hardware-based networks and testbeds should require no changes to code or configuration.
        
        \item Interactive; % managing and running the network should occur in real time, as if interacting with a real network. 
        
        \item Scalable; % the prototyping environment should scale to networks with hundreds or thousands of switches on only a laptop. 
        
        \item Realistic; % prototype behavior should represent real behavior with a high degree of confidence; for example, applications and protocol stacks should be usable without modification.
        
        \item Shareable. % self-contained prototypes should be easily shared with collaborators, who can then run and modify our experiments.
 
    \end{itemize}
    
\end{frame}


\begin{frame}
    \frametitle{Emulation approaches}
    
    \begin{itemize}
        \item Special-purpose testbeds (GENI, VINI, PlanetLab, Emulab).
 
        \item Simulators (ns-2, fs).
        
        \item Network of virtual machines.
        
        \item OS-level virtualization (Mininet, IMUNES).
        
    \end{itemize}

\end{frame}  


\subsection{Simulators}

\begin{frame}
    \frametitle{Notable simulators}
      
    \begin{itemize}
        \item ns-2 -- network simulator (1997).
        \item fs -- flow simulator (2011).
    \end{itemize}
    
     fs-sdn = fs + POX (OpenFlow controller) + OpenFlow ``switches''
\end{frame}  


\subsection{OS-level virtualization}

\begin{frame}
    \frametitle{Mininet}

    Benefits:
    \begin{itemize}
        \item Lightweight.
            Implementation via processes running in network namespaces and virtual Ethernet pairs (Linux).  %Veth pairs  may be attached to virtual switches such as the Linux bridge or a software OpenFlow switch.
        \item User friendly:
        \begin{itemize}
            \item distributed as a VM image;
            \item excellent CLI and Python API;
            \item some GUI tools.
        \end{itemize}
    \end{itemize}

    Limitations:
    \begin{itemize}
        \item Linux scheduler; % lack of performance fidelity, especially at high loads. CPU resources are multiplexed in time by the default Linux scheduler, which provides no guarantee that a host that is ready to send a packet will be scheduled promptly,
        \item software performance O(n) lookup vs hardware O(1);
        \item single kernel and file system. 
    \end{itemize}

\end{frame}  


\begin{frame}
    \frametitle{Mininet applications}
    \framesubtitle{}
    
    \centering
    \begin{minipage}{.45\textwidth}
        \includegraphics[width=\linewidth]{miniedit2108.png} 
        
        MiniEdit
    \end{minipage}
    \begin{minipage}{.45\textwidth}
        \includegraphics[width=\linewidth]{consoles_py.jpg}
        
        console.py 
    \end{minipage}

\end{frame}  




\begin{frame}
    \frametitle{IMUNES}
    \framesubtitle{ Integrated Multiprotocol Network Emulator/Simulator }
    \begin{itemize}
        \item Distributed as a VM image.
        \item Based on FreeBSD.
        \item GUI from the box.
    \end{itemize}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{imunes.png}     
    \end{center}
\end{frame}  




\begin{frame}
    \frametitle{Hybrids}

    \begin{itemize}
        \item CORE (Common Open Research Emulator).
        \item Supercharged PlanetLab.
    \end{itemize}
\end{frame}  


\subsection{Emulation comparison}

\begin{frame}
    \frametitle{Comparison}
        \begin{center}
            \begin{tabular}{|c|c|c|c|c|}
                \hline
                ~ & testbeds & simulators & VM & OS-level\\
                \hline
                  Flexible & - & + & - & + \\
                  Deployable & + & +/- & + & + \\
                  Interactive & + & +/- & + & + \\
                  Scalable & - & + & - & + \\
                  Realistic & + & - & + & +/- \\
                  Sharable & - & + & +/- & + \\
                \hline
            \end{tabular}
        \end{center}
\end{frame}  








\end{document}
