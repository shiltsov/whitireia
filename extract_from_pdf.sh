#!/usr/bin/env bash

#gs -g4800x3700 -sOUTPUTFILE=./test.pdf -sDEVICE=pdfwrite -dBATCH -c "<< /PageOffset [0 -85] >> setpagedevice" -f output2.pdf

set -o nounset
set -o errexit

[[ $# == 0 || $1 == --help ]] && echo "Usage: $0 INPUT OUTPUT SIZE_H SIZE_V OFFSET_H OFFSET_V" && exit 0;

readonly INPUT_FILE="$1"
readonly OUTPUT_FILE="$2"
readonly SIZE_H="$3"
readonly SIZE_V="$4"
readonly OFFSET_H="$5"
readonly OFFSET_V="$6"

gs -g$SIZE_H"x"$SIZE_V \
    -sOUTPUTFILE=$OUTPUT_FILE \
    -sDEVICE=pdfwrite \
    -dBATCH -c "<< /PageOffset ["$OFFSET_H $OFFSET_V"] >> setpagedevice" \
    -f$INPUT_FILE



